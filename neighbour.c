#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include "LAS_structures.h"
#include "read_las.h"

/**
 *  Assumes points sorted by elevation. 
 *  Three seed pixels, located by input percentile (or x,y coordinates).
 *  Assigns probability to other points based on Euclidean distance from start point.
 *  
 **/

int main (int argc, const char * argv[]) {
	FILE *file_in, *file_counts;
	FILE *high_prob, *low_prob, *no_prob;
	FILE *record;
	char layer[100]; // text file name
	char ext1[]=".txt"; // text file extension
	char ext2[]="-high"; // text file extension
	char ext3[]="-low"; // text file extension
	char ext4[]="-no"; // text file extension
	
	Header1 las_head1; // system info etc
	Header2 las_head2; // point transformation info
	Rec_head rec_head; // point record info
	Point1 point;	

	double refE, refN, refZ; // reference point coordinates
	//long int maxE_l, minE_l, maxN_l, minN_l;
	long int capZ_high, capZ_low;
	double realZ, delta_d, delta_z;
	int i, j, k, set=0; 
	int highs=0, lows=0, multis=0; 
	int onep, twop, threep, prob;
	int mode,eq;
	//float inX, inY;
	float iper;
	
	int layers, rows, cols;
	int l1, l2, r1, r2, c1, c2, start, end;
	int l, r, c;
	int ***counts;
	int ***accum;
	
	float temp, height, length, width;
	
	unsigned char ret = 0x07, num = 0x38; // identify bits for return number and number of returns
	unsigned char rn, nr; // point return number (from) number of returns 

	
	struct tm *ptr;
	time_t tm;
	tm = time(NULL);
	ptr = localtime(&tm);
	
	// input file: las; 
	if((argc !=5) && (argc !=8)){
		printf("usage %s  <input> <output> <mode> <equation> [per1 per 2 per3]\n", argv[0]);
		printf("\tmode 1 = percentage; mode 2 = specific coordinates (will be prompted)\n");
		printf("\tequation 1 = linear; equation 2 = negative quadratic (increased prob); equation 3 = positive quadratic (decreased prob)\n");
		printf("optional specify (float) percentile in for seed points\n");
		return 0;
	}
	
	
	record = fopen("/Users/km.amolins/code/neighbour_log.txt","a");
	fprintf(record, asctime(ptr));
	fprintf(record, "%s %s %s",argv[0], argv[1], argv[2]); // function
	if(argc == 7)
		fprintf(record, "%s %s %s",argv[4], argv[5], argv[6]); // function
	fprintf(record, "\n");
	
	// open LAS file
	file_in = fopen(argv[1], "rb"); 
	if(file_in == NULL){printf("unable to open input file\n"); return 1; }
	
	// read LAS headers
	fread(&las_head1, sizeof(Header1),1, file_in);
	read_recHead(file_in, &rec_head);
	fread(&las_head2, sizeof(Header2),1, file_in);
	print_lasHead(las_head1, las_head2, rec_head);
	
	// open voxel counts
	strcpy(layer, argv[1]);
	strcat(layer, ext1);
	file_counts = fopen(layer, "r");
	if(file_counts == NULL){printf("unable to open file %s\n",layer); return 1;}
	printf("Successfully opened file %s\n", layer);
	
	fscanf(file_counts, "%d\t%f\t%d\t%f\t%d\t%f\n",&layers,&height,&rows,&length,&cols,&width);
	printf("%d layers, %d rows, %d columns\n",layers, rows, cols);
	printf("height %f, length %f, width %f\n",height, length, width);
	
	
	// output file
	strcpy(layer, argv[2]);
	strcat(layer, ext2);
	//high_prob = fopen("temp/high_prob", "w+b" );
	high_prob = fopen(layer, "w+b" );
	if(high_prob == NULL){ printf("unable to open file high_prob\n"); return 1;} 
	printf("Successfully opened output file high prob\n");
	
	strcpy(layer, argv[2]);
	strcat(layer, ext3);
	//low_prob = fopen("temp/low_prob", "w+b" );
	low_prob = fopen(layer, "w+b" );
	if(low_prob == NULL){ printf("unable to open file low_prob\n"); return 1;} 
	printf("Successfully opened output file low prob\n");

	strcpy(layer, argv[2]);
	strcat(layer, ext4);
	//no_prob = fopen("temp/no_prob", "wb" );
	no_prob = fopen(layer, "w+b" );
	if(no_prob == NULL){ printf("unable to open file no_prob\n"); return 1;} 
	printf("Successfully opened output file no prob\n");
	
	// allocate memory
	counts = (int ***) calloc(layers, sizeof(int **));
	if(!(counts)){printf("calloc error - layers\n"); return 1;}
	for(i=0; i<layers; i++){
		counts[i] = (int **) calloc(rows, sizeof(int *));
		if(!(counts[i])){printf("calloc error - layer %d", i); return 1;}
		for(j=0; j<cols; j++){
			counts[i][j] = (int *) calloc(cols, sizeof(int));
			if(!(counts[i][j])){printf("calloc error - layer %d row %d", i,j); return 1;}
		} 
	}
	
	accum = (int ***) calloc(layers, sizeof(int **));
	if(!(accum)){printf("calloc error - layers\n"); return 1;}
	for(i=0; i<layers; i++){
		accum[i] = (int **) calloc(rows, sizeof(int *));
		if(!(accum[i])){printf("calloc error - layer %d", i); return 1;}
		for(j=0; j<cols; j++){
			accum[i][j] = (int *) calloc(cols, sizeof(int));
			if(!(accum[i][j])){printf("calloc error - layer %d row %d", i,j); return 1;}
		} 
	}
	
	
	for(i=0; i<layers; i++){
		fscanf(file_counts, "%f\n", &temp);
		printf("%d Layer %f\n",i, temp);
		for(j=0; j<rows; j++){
			fscanf(file_counts,"\t%f\n", &temp);
			//printf("\t%d Row %f\n",j, temp);
			for(k=0; k<cols; k++){
				fscanf(file_counts, "\t\t%f\t%d\t%d\n", &temp, &accum[i][j][k], &counts[i][j][k]);
				//printf("\t%d", k);
			}
			//printf("\n");
		}
	}
	printf("Counts read in.\n");
	
	eq = atoi(argv[4]);
	if(eq == 1)
		printf("linear\n");
	if(eq == 2)
		printf("negative quad\n");
	if(eq==3)
		printf("positive quad\n");
	if(eq !=1 && eq !=2 && eq != 3){
		printf("invalid eqaution mode\n");
		return 1;
	}
		
	
	mode = atoi(argv[3]);
	// get seed pixels	

	if(mode == 1){
		if(argc == 5){
			printf("Input first percentage (whole number): ");
			scanf("%f", &iper);
		}
		else
			iper = atof(argv[5]);
		
		onep = (int) floor(rec_head.num_points/100)*iper;
	
		fseek(file_in, las_head1.offset + onep*rec_head.rec_len, 0);
		while(set == 0){
			fread(&point, rec_head.rec_len,1, file_in);
			rn = point.bits & ret; // find return number
			nr = (point.bits & num) >>3; // find number of returns
			if(rn == nr) 
				set++;
			else
				onep++;
		}
		printf("First ground seed at %d\n", onep);
		realZ = 1.0 * point.z * las_head2.z_scale + las_head2.z_offset;
		refE = 1.0 * point.x * las_head2.x_scale;
		refN = 1.0 * point.y * las_head2.y_scale;
		refZ = 1.0 * point.z * las_head2.z_scale;
	
		point.user_data=100;
		fwrite(&point, rec_head.rec_len, 1, high_prob);
		highs++;
	
		printf("%0.3f -> %0.3f, ", refE, refE + las_head2.x_offset);
		printf("%0.3f -> %0.3f, ", refN, refN + las_head2.y_offset);
		printf("%0.4f = %0.4f\n", refZ, realZ);
		printf("\t%d, source %d scan %d user %d\n", point.i, point.p_sourceID, point.angle, point.user_data);
	
		fprintf(record, "First ground seed %0.3f %0.3f %0.4f\n", refE + las_head2.x_offset, refN + las_head2.y_offset, realZ);
		
		set = 0;
		// find more ground seeds
		if(argc == 5) {
			printf("Input second percentage (whole number): ");
			scanf("%f", &iper);
		}
		else
			iper = atof(argv[6]);
	
		twop = (int) floor(rec_head.num_points/100)*iper;
		fseek(file_in, las_head1.offset + twop*rec_head.rec_len, 0);
		while(set == 0){
			fread(&point, rec_head.rec_len,1, file_in);
			rn = point.bits & ret; // find return number
			nr = (point.bits & num) >>3; // find number of returns
			if(rn == nr) 
				set++;
			else
				twop++;
		}
		point.user_data=100;
		fwrite(&point, rec_head.rec_len, 1, high_prob);
		highs++;
	
		printf("Second ground seed at %d\n", twop);
		printf("%0.3f -> %0.3f, ", 1.0 * point.x * las_head2.x_scale, 1.0 * point.x * las_head2.x_scale + las_head2.x_offset);
		printf("%0.3f -> %0.3f, ", 1.0 * point.y * las_head2.y_scale, 1.0 * point.y * las_head2.y_scale + las_head2.y_offset);
		printf("%0.4f = %0.4f\n", 1.0 * point.z * las_head2.z_scale, 1.0 * point.z * las_head2.z_scale + las_head2.z_offset);
		printf("\t%d, source %d scan %d user %d\n", point.i, point.p_sourceID, point.angle, point.user_data);
	
		fprintf(record, "Second ground seed %0.3f %0.3f %0.4f\n", 1.0 * point.x * las_head2.x_scale + las_head2.x_offset, 1.0 * point.y * las_head2.y_scale + las_head2.y_offset, 1.0 * point.z * las_head2.z_scale + las_head2.z_offset);
	
	
		set = 0;
		if(argc == 5){
			printf("Input third percentage (whole number): ");
			scanf("%f", &iper);
		}
		else
			iper = atof(argv[7]);
		
		threep = (int) floor(rec_head.num_points/100)*iper;
		fseek(file_in, las_head1.offset + threep*rec_head.rec_len, 0);
		while(set == 0){
			fread(&point, rec_head.rec_len,1, file_in);
			rn = point.bits & ret; // find return number
			nr = (point.bits & num) >>3; // find number of returns
			if(rn == nr) 
				set++;
			else
				threep++;
		}
		point.user_data=100;
		fwrite(&point, rec_head.rec_len, 1, high_prob);
		highs++;	
	
		printf("Third ground seed at %d\n", threep);
		printf("%0.3f -> %0.3f, ", 1.0 * point.x * las_head2.x_scale, 1.0 * point.x * las_head2.x_scale + las_head2.x_offset);
		printf("%0.3f -> %0.3f, ", 1.0 * point.y * las_head2.y_scale, 1.0 * point.y * las_head2.y_scale + las_head2.y_offset);
		printf("%0.4f = %0.4f\n", 1.0 * point.z * las_head2.z_scale, 1.0 * point.z * las_head2.z_scale + las_head2.z_offset);
		printf("\t%d, source %d scan %d user %d\n", point.i, point.p_sourceID, point.angle, point.user_data);
	
		fprintf(record, "Third ground seed %0.3f %0.3f %0.4f\n", 1.0 * point.x * las_head2.x_scale + las_head2.x_offset, 1.0 * point.y * las_head2.y_scale + las_head2.y_offset, 1.0 * point.z * las_head2.z_scale + las_head2.z_offset);
		//return 1;
	} // end mode 1
	else{
		// assume else mode 2
		
		printf("Not yet implemented\n");
		return 1;
		
	}
	
	i=0;
	j=0;
	k=0;
	// initial 
	fseek(file_in, las_head1.offset,0);
	while(i< rec_head.num_points){	
		//while(i<10){
				
		fread(&point, rec_head.rec_len,1, file_in);
		if(i== onep || i == twop || i == threep){ // skip over seed points
			i++;
			fwrite(&point, rec_head.rec_len, 1, low_prob);	// keep in low_prob file so don't mess up counts	
			continue;
		}	
		
		rn = point.bits & ret; // find return number
		nr = (point.bits & num) >>3; // find number of returns
		//printf("%0x, %d of %d\n", point.bits, rn, nr);
		
		if(rn == nr){ 
			// only singles or lasts
			delta_d = sqrt(pow(point.x*las_head2.x_scale - refE,2)+pow(point.y*las_head2.y_scale - refN,2));
			delta_z = fabs(point.z*las_head2.z_scale - refZ);  
			
			// initial probability that ground based on vertical and horizontal distance from first seed point
			if(eq == 2) // negative quadratic, higher probabilities
				prob = 100 - 25 * pow(delta_z + delta_d/2, 2);
			
			if(eq == 3) {// positive quadratic, lower probabilities
				if((delta_d/3 + delta_z )<= 2)
					prob = pow(5.0*(delta_z + delta_d/3)-10.0, 2);
				else
					prob = 0; // artificially tamp out rising quad
			}
			
			if(eq == 1) // linear
				prob = 100 - 50 * (delta_z + delta_d/2);
			
			
			if(prob < 0)
				prob = 0;
			if(prob > 100)
			   prob = 100;
			
			if(prob > point.user_data)
				point.user_data = prob; // record highest probability
		
		
			//if(prob >= 90 ){
			if(prob >= 50 ){ // keep any with probability 50% or more as new seed points
				fwrite(&point, rec_head.rec_len, 1, high_prob);
				fwrite(&point, rec_head.rec_len, 1, low_prob); // for counting purposes
				highs++;
			}
			else{ // store low probability for future passes
				fwrite(&point, rec_head.rec_len, 1, low_prob);
				lows++;
			}
			
		
			if(delta_d < 2 && (delta_d != 0 && delta_z !=0)){ // print out all points within 2 m (horizontal) of first seed
				printf("%d\t", i);
				printf("%0.3f\t", point.x*las_head2.x_scale - refE);
				printf("%0.3f\t", point.y*las_head2.y_scale - refN);
				printf("%0.3f\t", point.z*las_head2.z_scale - refZ);
			   printf("%0.3f %0.4f\t%d\n", delta_d, delta_z, prob);

				//printf("%f\t%d\n", distance, prob);
			}	
			
		}
		else{ // not single/last
			point.user_data = 'm'; // mark it as multi
			fwrite(&point, rec_head.rec_len, 1, low_prob);	// store for counting		
			fwrite(&point, rec_head.rec_len, 1, no_prob); // store in "not ground" file
			multis++;
		}
					
		i++;
	} // reached end of las file
	fclose(file_in);
	i=1; // point to next seed
	
	printf("highs = %d; lows = %d; multis = %d\n", highs, lows, multis);
	fprintf(record, "Very likely %d; potentials %d; multiples %d\n", highs, lows, multis);
	
	//return 1;
	
	// cycle through points marked high; this count keeps incrementing until no more neighbours found
	while(i<highs){
		fseek(high_prob, i * rec_head.rec_len,0); // jump to next seed/high
		fread(&point, rec_head.rec_len,1, high_prob); // read in next flagged ground point
		realZ = 1.0 * point.z * las_head2.z_scale + las_head2.z_offset; // scaled and offset z
		refE = 1.0 * point.x * las_head2.x_scale; // scaled coordinates
		refN = 1.0 * point.y * las_head2.y_scale;
		refZ = 1.0 * point.z * las_head2.z_scale;
		capZ_high = (long int) point.z + 2 / las_head2.z_scale; // unscaled limits; no more than 2 m above/below
		capZ_low = (long int) point.z - 2 / las_head2.z_scale;
		
		i++; // point to next seed/high
		k=0; // k not important?
		j=0;
		
		// find columns, rows, layers containing 2 m cube around current flagged ground point
		c1 = (int) floor((refE + las_head2.x_offset - 3 - las_head2.x_min)/width);
		r1 = (int) floor((refN + las_head2.y_offset - 3 - las_head2.y_min)/length);
		l1 = (int) floor((refZ + las_head2.z_offset - 3 - las_head2.z_min)/height);
		c2 = (int) floor((refE + las_head2.x_offset + 3 - las_head2.x_min)/width);
		r2 = (int) floor((refN + las_head2.y_offset + 3 - las_head2.y_min)/length);
		l2 = (int) floor((refZ + las_head2.z_offset + 3 - las_head2.z_min)/height);
		
		
		// check within bounds
		if(c1<0)
			c1=0;
		if(r1<0)
			r1=0;
		if(l1<0)
			l1=0;
		
		if(c2>=cols)
			c2=c1;
		if(r2>=rows)
			r2=r1;
		if(l2>=layers)
			l2=l1;
		
		for(l = l1; l<=l2; l++){
			for(r = r1; r<=r2; r++){
				for(c = c1; c<=c2; c++){
					start = accum[l][r][c]; 
					end = accum[l][r][c] + counts[l][r][c];
				
			// use point accumulations and counts to mark where to start/end looking in low_prob for neighbouring points
			// repeat for each column, row, layer for contained cube
		
			
		j=start; 
		fseek(low_prob, j * rec_head.rec_len,0); // jump to starting point
		
		while(j < end){
				fread(&point, rec_head.rec_len,1, low_prob);
		
	
			if(point.user_data == 104){ // high probability and already tested, skip
				j++;
				continue;
			}
			if(point.user_data == 'm'){ // multiple returns, skip
				j++;
				continue;
			}
			
						
			if(point.z <= capZ_high && point.z >= capZ_low){ // within identified vertical limits
				// calculate vertical and horizontal distance from current flagged, find probability
				delta_d = sqrt(pow(point.x*las_head2.x_scale - refE,2)+pow(point.y*las_head2.y_scale - refN,2));
				delta_z = fabs(point.z*las_head2.z_scale - refZ);
				
				if(eq == 2) // negative quadratic, higher probabilities
					prob = 100 - 25 * pow(delta_z + delta_d/2, 2);
				if(eq == 3) {// positive quadratic, lower probabilities
					if((delta_d/3  + delta_z) <=2)
						prob = pow(5.0*(delta_z + delta_d/3)-10.0, 2);
					else
						prob = 0; // artificially tamp out rising quad
				}
				if(eq == 1) // linear
					prob = 100 - 50 * (delta_z + delta_d/2);
				
				if(prob < 0)
					prob = 0;
				if(prob > 100)
					prob = 100;
				
				if(prob > point.user_data)
					point.user_data = prob; // take highest probability
			
				// check probability
				if(prob >= 50){ 
					fseek(high_prob, highs * rec_head.rec_len,0); // jump to end of high file
					fwrite(&point, rec_head.rec_len, 1, high_prob); // store as new flagged ground
					highs++; // increment number of flagged points
					
					fseek(low_prob, j * rec_head.rec_len,0); // rewind one point
					point.user_data = 104;
					fwrite(&point, rec_head.rec_len, 1, low_prob); // mark it out in low file so skipped in future loops
				}
				else{ // not high enough probablity
					k++;
				}
			} // end z restrictions

			else{ // not within z limits; move on
				k++;
			} // end outside z restrictions
		
			
			j++; // j counts from start to end for cube
		} // end while j < end
				} // end extra columns
			} // end extra rows
		} // end extra layers
		// finished checking for current flagged, move on to next if any
		

	} // end while highs, checked all points flagged as ground
	
	// lows is all points minus multiples; remainder is therefore points that aren't multis and aren't ground
	fprintf(record, "Flagged %d; remainder %d\n", highs, lows - highs);
	printf("Flagged %d; remainder %d\n", highs, rec_head.num_points - highs - multis);
	printf("pt2grid %s %s - high <output> %d\n", argv[1], argv[2], highs);
		 
	
	fclose(high_prob);
	fclose(low_prob);
	
	tm = time(NULL);
	ptr = localtime(&tm);
 	fprintf(record, asctime(ptr));
	fprintf(record, "\n--- END ---\n");
	fclose(record);
	
    return 0;
}
