#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include "esri_hdr.h"

/* Remove salt and pepper: 
 * mode 0 (regular) if fewer than 3 have values >=1, set to 0; if more than 5 marked, set to 2; 3 to 5 set to 1 (optional).
 * mode 1 (ground-vegetation ratio) if 100, set to 2; if touching 100, set to 1; else set to 0.
 * mode 2 (ruggedness) if <1 set to 0 if touching <1 set to 1; else set to 2.
 */

int main (int argc, const char * argv[]) {
	FILE *hdr_in, *bin_in, *hdr_out, *bin_out; // actual 
	ESRI_hdr esri_hdr; // grid file header
	char header[100], binary[100]; // file names
	char ext1[]=".hdr", ext2[]=".bil"; // file extensions
	
	float **input_f;
	int **input_i;
	int **hold;
	int i, j, k,l;
	float rows, cols;
	int count;
	int mode, intype, one=1;
	float in_ij, in_kl;
	
	if(argc != 5 && argc !=6){
		printf("usage %s  <input> <output> <mode> <input type> [ref header]\n", argv[0]);
		printf("where all files are ESRI binary rasters, omit extension\n");
		printf("mode 0 regular; mode 1 ground-vegetation ratio; mode 2 ruggedness\n");
		printf("input type 0 for float, 1 for int\n");
		printf("if input file has new esri header, provide reference header\n");
		return 0;
	}
	
	mode = atoi(argv[3]);
	intype = atoi(argv[4]);
	
	//open header files
	if(argc == 5) {
	strcpy(header, argv[1]);
	strcat(header, ext1);
	hdr_in = fopen(header, "r");
	if(hdr_in == NULL){printf("unable to open file %s\n",header); return 1;}
	printf("Successfully opened file %s\n", header);
	}
	else{
		strcpy(header, argv[5]);
		strcat(header, ext1);
		hdr_in = fopen(header, "r");
		if(hdr_in == NULL){printf("unable to open file %s\n",header); return 1;}
		printf("Successfully opened file %s\n", header);
	}
	 
	
	strcpy(header, argv[2]);
	strcat(header, ext1);
	hdr_out = fopen(header, "w");
	if(hdr_out == NULL){printf("unable to open file %s\n",header); return 1;}
	printf("Successfully opened file %s\n", header);
	
	// read header
	read_esri_hdr(hdr_in, &esri_hdr);
	print_esri_hdr(esri_hdr);
	rows=esri_hdr.nrows;
	cols=esri_hdr.ncols;
	esri_hdr.nodata_value = -9999;
	
	// write esri header
	write_esri_int(hdr_out, &esri_hdr);
	
	// close header files
	fclose(hdr_in);
	fclose(hdr_out);
	
	//open binary files	
	// input
	strcpy(binary, argv[1]);
	strcat(binary, ext2);
	bin_in = fopen(binary, "rb");
	if(bin_in == NULL){printf("unable to open file %s\n",binary); return 1;}
	printf("Successfully opened file %s\n", binary);
	
	// output
	strcpy(binary, argv[2]);
	strcat(binary, ext2);
	bin_out = fopen(binary, "wb");
	if(bin_out == NULL){printf("unable to open file %s\n",binary); return 1;}	
	printf("Successfully opened file %s\n", binary);
		
	// allocate memory input
	if(intype == 0){
		input_f = (float **) calloc(rows, sizeof(float *));
		if(!(input_f)){printf("calloc error - rows\n"); return 1;}
		for(i=0; i<rows; i++){
			input_f[i] = (float *) calloc(cols, sizeof(float));
			if(!(input_f[i])){printf("calloc error - row %d",i); return 1;}
		}
	}
	else{
		input_i = (int **) calloc(rows, sizeof(int *));
		if(!(input_i)){printf("calloc error - rows\n"); return 1;}
		for(i=0; i<rows; i++){
			input_i[i] = (int *) calloc(cols, sizeof(int));
			if(!(input_i[i])){printf("calloc error - row %d",i); return 1;}
		}
	}
	
	// allocate memory hold 
	hold = (int **) calloc(rows, sizeof(int *));
	if(!(hold)){printf("calloc error - rows\n"); return 1;}
	for(i=0; i<rows; i++){
		hold[i] = (int *) calloc(cols, sizeof(int));
		if(!(hold[i])){printf("calloc error - row %d",i); return 1;}
	}
	printf("Memory allocated\n");
	
	// read in
	for(i=0;i<rows;i++){
		for(j=0;j<cols;j++){
			if(intype ==0)
				fread(&input_f[i][j], sizeof(float), 1, bin_in);
			else
				fread(&input_i[i][j], sizeof(int), 1, bin_in);
		}
	} 
	printf("Data read in. Type %d\n", intype);
	
	fclose(bin_in);
	
	if(mode == 0){
		printf("Output 1's? yes = 1 (default) no = 0 ");
		scanf("%d", &one);
	}
	
	for(i=0; i<rows; i++){
		for(j=0; j<cols; j++){
			count = 0;
			hold[i][j] = -1;
			if(intype == 0)
				in_ij = input_f[i][j];
			else
				in_ij = input_i[i][j];
			
			if(in_ij < 0)
				in_ij = esri_hdr.nodata_value;
			
			
			if(mode == 0){ // regular
				if(i==0 || j==0 || i==(rows-1) || j==(cols-1) )
					hold[i][j] = 0; // outer rim
				else{
					for(k=-1; k<2; k++){
						for(l=-1; l<2; l++){
							if(intype == 0)
								in_kl = input_f[i+k][j+l];
							else
								in_kl = input_i[i+k][j+l];
							
							
							if(in_kl >= 1)
								count++;
						} // end l
					} // end k
				
					if(count >= 6){
						hold[i][j] = 2; // set as marked
					
					}
					if(count <=2){
						hold[i][j] = 0; // set as unmarked
					
					}
					if(count>2 && count <6){ // in between
						
						if(one)
							hold[i][j] = 1;
						else{  // keep original value
							if(in_ij > 0)
								hold[i][j] = 2;
							else
								hold[i][j] = 0;   
							
						}
							
												
					}
				
				} // end not outer rim
			} // end mode 0
			if(mode ==1) { // ground-vegetation ratio
				if(in_ij == 100)
					hold[i][j] = 9; // surroundings only high vegetation
				else{ // not itself only veg
					for(k=-1; k<2; k++){
						if(i+k < 0 || i+k >= rows)
							continue; // outside rows
						
						for(l=-1; l<2; l++){
							if(j+l < 0 || j+l >= cols)
								continue; // outside cols
							
							if(intype == 0)
								in_kl = input_f[i+k][j+l];
							else
								in_kl = input_i[i+k][j+l];
							
							
							if(in_kl == 100)
								count++; // touching high vegetation cell
						} // end l
					} // end k
				
					hold[i][j] = count;
		
				}
				
			} // mode 1
			if(mode ==2) { // ruggedness
					for(k=-1; k<2; k++){
						if(i+k < 0 || i+k >= rows)
							continue; // outside rows
						
						for(l=-1; l<2; l++){
							if(j+l < 0 || j+l >= cols)
								continue; // outside cols
							
							if(intype == 0)
								in_kl = input_f[i+k][j+l];
							else
								in_kl = input_i[i+k][j+l];
							
							
							if(in_kl < 1.5)
								count++; // touching low ruggedness cell
						} // end l
					} // end k
					
				hold[i][j] = count;
				//}
				
			} //mode 2
			
		}//end j
	} // end i

	
	// write data to file
	for(i=0; i<rows; i++){
		for(j=0; j<cols; j++){
			fwrite(&hold[i][j], sizeof(int), 1, bin_out);
		}
	}
	printf("Data written\n");
	if(intype == 0)
		free(input_f);
	else
		free(input_i);
	
	free(hold);
	
	// close binary files
	fclose(bin_out);
	
	
	return 0;
}
