#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "esri_hdr.h"

/****************************
 * Applies mask created from wavelet details to original surface.
 * Mask values 0 = keep original; 1 = nodata
 * Mask is in new esri format, so use original header as base
 *
 ****************************/

int main (int argc, const char * argv[]) {
    FILE *hdr_in, *bin_in, *hdr_out, *bin_out, *mask;
	ESRI_hdr esri_hdr;
	char header[100], binary[100]; // file names
	char ext1[]=".hdr", ext2[]=".bil"; // file extensios
	float ori, m, out; // data
	int i, j;

	// command line: (name) (input file) (output file) (mask)
	if(argc !=4){
		printf("usage: %s <input> <output> <mask>\n", argv[0]);
		printf("files are ESRI binary rasters, omit extension\n");
		printf("mask values 0 = keep original, 1 = make nodata\n");
		return 0;
	}
	
	//open header files
	strcpy(header, argv[1]);
	strcat(header, ext1);
	hdr_in = fopen(header, "r");
	if(hdr_in == NULL){printf("unable to open file %s\n",header); return 1;}
	printf("Successfully opened file %s\n", header);
	
	strcpy(header, argv[2]);
	strcat(header, ext1);
	hdr_out = fopen(header, "w");
	if(hdr_out == NULL){printf("unable to open file %s\n",header); return 1;}
	printf("Successfully opened file %s\n", header);
	
	// read header
	read_esri_hdr(hdr_in, &esri_hdr);
	print_esri_hdr(esri_hdr);
	
	// write esri header
	write_esri_hdr(hdr_out, &esri_hdr);
	
	// close header files
	fclose(hdr_in);
	fclose(hdr_out);
	
	//open binary files	
	// input, original surface
	strcpy(binary, argv[1]);
	strcat(binary, ext2);
	bin_in = fopen(binary, "rb");
	if(bin_in == NULL){printf("unable to open file %s\n",binary); return 1;}
	printf("Successfully opened file %s\n", binary);
	
	// mask
	strcpy(binary, argv[3]);
	strcat(binary, ext2);
	mask = fopen(binary, "rb");
	if(mask == NULL){printf("unable to open file %s\n",binary); return 1;}
	printf("Successfully opened file %s\n", binary);
	
	// output
	strcpy(binary, argv[2]);
	strcat(binary, ext2);
	bin_out = fopen(binary, "wb");
	if(bin_out == NULL){printf("unable to open file %s\n",binary); return 1;}	
	printf("Successfully opened file %s\n", binary);
	
	// read data into grid
	for(i=0; i<esri_hdr.nrows; i++){
		for(j=0; j<esri_hdr.ncols; j++){
			fread(&ori, sizeof(float), 1, bin_in);
			fread(&m, sizeof(float), 1, mask);
			if(m == 0)
				out = ori; // keep original
			else
				out = esri_hdr.nodata_value; // mask original value
		
			fwrite(&out, sizeof(float), 1, bin_out);
		}
	}
	
	printf("Data masked.\n");
	
	// close binary files
	fclose(bin_in);
	fclose(bin_out);
	fclose(mask);
	
  return 0;
}
