#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "las_structures.h"
#include "read_las.h"
#include "esri_hdr.h"

/* Interpolates LAS point data to ESRI grid.
 * User specified cellsize
 * Interpolation methods: 
 *    1 - average elevation
 *    2 - average intensity
 *    3 - inverse distance weighting (elevation)
 *    4 - average elevation of one class
 *    5 - average elevation for one flight line
 *    6 - point density -> source ID must be flight line number; max 14 lines
 *    7 - count non-single (i.e., multiple) returns
 *    8 - minimum value
 *    9 - maximum value
 *   10 - count number of class n within area
 *   11 - user data; to be used after "grow_patches"
 *  Valid points are within area equal to:
 *		radius factor 0 = grid cell
 *		radius factor 1 = 1/2, circular search area contained to cell area
 *		radius factor 2 = circular search area equal to cell area
 *		radius factor 3 = root2/2, circular search area contains cell area
 *  For (3) user prompted for power parameter, asked if single class and if yes class number
 *  For (4) user prompted for class
 *  For (5) user prompted for flight line
 *  For (9) asked if max Z cap and if yes enter cap
 *  For (10) user prompted for class
 */

int main(int argc, char *argv[])
{
	FILE *file_in, *hdr_out, *bin_out;
	Header1 las_head1; // system info etc
	Header2 las_head2; // point transformation info
	Rec_head rec_head; // point record info
	Point1 point;
	ESRI_hdr esri_hdr; // grid file header
	char header[50], binary[50]; // file names
	char ext1[]=".hdr", ext2[]=".bil"; // file extensions
	float **grid, **sum, **num;
	int mode, radius, single;
	int i,j, k,l, count_no, count_over;
	double maxE, minE, maxN, minN; // limits
	int row, col;
	float distance, avg, p, mZ9, mZ8;
	double valid;
	char ***lines;
	unsigned char ret = 0x07, numb = 0x38, rn, nr; // point return number

  	
	if(argc != 6){
		printf("usage %s  <input> <output> <cellsize float> <mode> <radius factor>\n", argv[0]);
		printf("mode 1 - elevation; mode 2 - intensity (first returns); mode 3 - inverse distance weighting;\n");
		printf("mode 4 - one class; mode 5 - one flight line; mode 6 - local point density (ignore multi points) \n");
		printf("mode 7 - multiple returns \n");
		printf("mode 8 - minimum ; mode 9 - maximum\n");
		printf("mode 10 - number of class n within area\n");
		printf("mode 11 - user data, should select cellsize so one point per\n");
		printf("radius factor 0 = grid cell \n");
		printf("radius factor 1 = 1/2, circular search area contained to cell area \n");
		printf("radius factor 2 = circular search area equal to cell area \n");
		printf("radius factor 3 = root2/2, circular search area contains cell area \n");
		return 0;
	}

	// open LAS file
	file_in = fopen(argv[1], "rb"); 
	if(file_in == NULL){printf("unable to open input file\n"); return 1; }
	
	// read LAS headers
	fread(&las_head1, sizeof(Header1),1, file_in);
	read_recHead(file_in, &rec_head);
	fread(&las_head2, sizeof(Header2),1, file_in);
	print_lasHead(las_head1, las_head2, rec_head);
	
	// determine limits
	maxE = (las_head2.x_max - las_head2.x_offset)/las_head2.x_scale;
	minE = (las_head2.x_min - las_head2.x_offset)/las_head2.x_scale;
	printf("Transformed Eastings %f %f\n", minE, maxE);
	maxN = (las_head2.y_max - las_head2.y_offset)/las_head2.y_scale;
	minN = (las_head2.y_min - las_head2.y_offset)/las_head2.y_scale;
	printf("Transformed Northings %f %f\n\n", minN, maxN);

	// open ESRI header file
	strcpy(header, argv[2]);
	strcat(header, ext1);
	hdr_out = fopen(header, "w");
	if(hdr_out == NULL){printf("unable to open file %s\n",header); return 1;}
	printf("Successfully opened file %s\n", header);
	
	// calculate ESRI header values
	esri_hdr.cellsize = atof(argv[3]);
	esri_hdr.xllcorner = las_head2.x_min;
	esri_hdr.yllcorner = las_head2.y_min;
	esri_hdr.ncols = (float) ceil((las_head2.x_max - las_head2.x_min)/esri_hdr.cellsize);
	esri_hdr.nrows = (float) ceil((las_head2.y_max - las_head2.y_min)/esri_hdr.cellsize);
	esri_hdr.nodata_value = -9999.0;
	
	print_esri_hdr(esri_hdr);
	
	radius = atoi(argv[5]);
	if(radius == 0)
		valid = esri_hdr.cellsize; // holding value; only points within cell
	if(radius == 1) 
		valid = esri_hdr.cellsize/3.141592654; //circle within grid cell
	if(radius == 2 || radius < 0 || radius > 3) // default option 
		valid = sqrt(pow(esri_hdr.cellsize,2)/3.141592654); //circle with area equal to area of grid cell, origin at centre of cell
	if(radius == 3)
		valid = esri_hdr.cellsize * sqrt(2.0)/2; // grid cell within circle
		
	printf("valid = %f \n", valid);
	
	// write ESRI header 
	write_esri_hdr(hdr_out, &esri_hdr);
	
		
	
	// open ESRI binary file
	strcpy(binary, argv[2]);
	strcat(binary, ext2);
	bin_out = fopen(binary, "wb");
	if(bin_out == NULL){printf("unable to open file %s\n",binary); return 1;}
	printf("Successfully opened file %s\n", binary);
	
	mode = atoi(argv[4]);
	if(mode == 1)
		printf("Gridding elevations\n");
	if(mode == 2)
		printf("Gridding intensity\n");
	if(mode == 3){
		printf("Using inverse distance weighting\n");
		printf("Enter power parameter: ");
		scanf("%f", &p);
		printf("Single class? Enter 1 for yes, 0 for no: ");
		scanf("%d", &single);
		if(single){
			printf("Enter class number: ");
			scanf("%f", &p);
		}
	}
	if(mode == 4){
		printf("Gridding elevation of one class\n");
		printf("Enter class number: ");
		scanf("%f", &p);
	}
	if(mode == 5){
		printf("Gridding elevation of one flight line\n");
		printf("Enter flight line id (point source): ");
		scanf("%f", &p);
	}
	if(mode ==6){
		printf("Gridding point density\n");
	}
	if(mode ==7){
		printf("Gridding multiple returns, count strictly within cell\n");
	}
	if(mode ==8){
		printf("Gridding minimum values\n");
		printf("Single class? Enter 1 for yes, 0 for no: ");
		scanf("%d", &single);
		if(single){
			printf("Enter class number: ");
			scanf("%f", &p);
		}
		
	}
	if(mode ==9){
		printf("Gridding maximum values\n");
		printf("Cap maximum z? Enter 1 for yes, 0 for no: ");
		scanf("%d", &single);
		if(single){
			printf("Enter maximum z: ");
			scanf("%f", &mZ9);
		}
		
	}
	if(mode == 10){
		printf("Gridding count of one class\n");
		printf("Enter class number: ");
		scanf("%f", &p);
	}
	if(mode == 11)
		printf("Gridding user data values\n");
		
	if(mode<1 || mode > 11){
		printf("Invalid mode. Exiting.\n");
		return 1;
	}
	
	
	
	// allocate memory
	sum = (float **) calloc(esri_hdr.nrows, sizeof(float *));
	if(!(sum)){printf("calloc error - rows\n"); return 1;}
	for(i=0; i<esri_hdr.nrows; i++){
		sum[i] = (float *) calloc(esri_hdr.ncols, sizeof(float));
		if(!(sum[i])){printf("calloc error - row %d",i); return 1;}
	}
	
	if(mode == 6){
		lines = (char ***) calloc(esri_hdr.nrows, sizeof(char **));
		if(!(lines)){printf("calloc error - rows\n"); return 1;}
		for(i=0; i<esri_hdr.nrows; i++){
			lines[i] = (char **) calloc(esri_hdr.ncols, sizeof(char *));
			if(!(lines[i])){printf("calloc error - row %d", i); return 1;}
			for(j=0; j<esri_hdr.ncols; j++){
				lines[i][j] = (char *) calloc(14, sizeof(char));
				if(!(lines[i][j])){printf("calloc error - row %d col %d", i,j); return 1;}
			} // max 14 flight lines!
		}
	}
	else{
		if(mode != 8 && mode !=9){
		grid = (float **) calloc(esri_hdr.nrows, sizeof(float *));
		if(!(grid)){printf("calloc error - rows\n"); return 1;}
		for(i=0; i<esri_hdr.nrows; i++){
			grid[i] = (float *) calloc(esri_hdr.ncols, sizeof(float));
			if(!(grid[i])){printf("calloc error - row %d",i); return 1;}
		}
		//if(mode == 3){
		num = (float **) calloc(esri_hdr.nrows, sizeof(float *));
		if(!(num)){printf("calloc error - rows\n"); return 1;}
		for(i=0; i<esri_hdr.nrows; i++){
			num[i] = (float *) calloc(esri_hdr.ncols, sizeof(float));
			if(!(num[i])){printf("calloc error - row %d",i); return 1;}
		}
		} // end not 6, 8, or 9
	
		//}
	} // end not 6
	
	if(mode == 8){ // min, need to set cells high initially
		for(i=0; i<esri_hdr.nrows; i++){
			for(j=0; j<esri_hdr.ncols; j++){
				sum[i][j] = - esri_hdr.nodata_value; // assuming -9999.0
			}
		}
		
	}
	if(mode == 9){ // max, need to set cells low initially
		for(i=0; i<esri_hdr.nrows; i++){
			for(j=0; j<esri_hdr.ncols; j++){
				sum[i][j] = esri_hdr.nodata_value; // assuming -9999.0
			}
		}
		
	}
	
	printf("Memory allocated\n");
	
	fseek(file_in, las_head1.offset,0);
	
	count_over=0;
	j=0;
	//while(!(feof(file_in))){	
	while(j < rec_head.num_points){
		fread(&point, rec_head.rec_len,1, file_in);
		j++;
		
		// determine row and column of point 
		row = (int) floor(point.y -  minN)*las_head2.y_scale/esri_hdr.cellsize;
		col = (int) floor(point.x -  minE)*las_head2.x_scale/esri_hdr.cellsize;
		
		
		if(((mode == 3 && single) || mode == 4 || mode == 10 || (mode == 8 && single)) && point.p_class != p)
			continue; //  single class only
		
		if(mode == 5 && point.p_sourceID != p)
			continue; // not matching flight line
		
		for(k=-2; k<3; k++){
			if(radius == 0 && k !=0)
				continue; // only interested in cell
			
			for(l=-2; l<3; l++){
				if(radius == 0 && l !=0)
					continue; // only interested in cell
				
				if((row+k>=0 && row+k< esri_hdr.nrows) && (col+l>=0 && col+l< esri_hdr.ncols) ){

					distance=sqrt(pow((col+l+0.5)*esri_hdr.cellsize-(point.x-minE)*las_head2.x_scale,2) + pow((row+k+0.5)*esri_hdr.cellsize-(point.y-minN)*las_head2.y_scale, 2));
					if(distance< valid) {

						if(mode == 1 || mode == 4 || mode == 5 ){ // grid average elevation: all, one class, one flight line
							grid[row+k][col+l] += point.z*las_head2.z_scale; // metres
							sum[row+k][col+l]++;
						}
							
						if(mode == 2){ // grid intensity
							rn = point.bits & ret;
							if(rn == 1){ //only first returns
								if(point.i >200)
									point.i = 200; // smooth out intensity spikes
								grid[row+k][col+l] += point.i; // average intensity
								sum[row+k][col+l]++;
							}
						}
						
						if(mode == 3){ // inverse weighting
							// Sum(point value * weight) / Sum(weights)
							sum[row+k][col+l]++;
							grid[row+k][col+l] += 1.0/pow(distance,p); // metres: sum of all weights
							num[row+k][col+l] += point.z*las_head2.z_scale / pow(distance,p); // metres: sum of all weighted point
							
						}
						
						if(mode == 6){ // point density
							rn = point.bits & ret;
							if(rn == 1) { // ignore multiple returns beyond first return
								sum[row+k][col+l]++; 
								lines[row+k][col+l][point.p_sourceID]=1; // sourceID MUST be line number
							}
						}
						
						if(mode == 7){ // multiple returns
							rn = point.bits & ret;
							nr = (point.bits & numb) >>3;
							if (nr == 1)
								continue; // point is single return
							else
								sum[row+k][col+l]++;
						} 
						
						if(mode == 8){ // minimum value
							mZ8 = point.z*las_head2.z_scale + las_head2.z_offset;
							if( mZ8 < sum[row+k][col+l])
								sum[row+k][col+l] = mZ8;
						}

						if(mode == 9){ // maximum value
							p = point.z*las_head2.z_scale + las_head2.z_offset;
							if(single && p > mZ9)
								continue; // maximum z cap exceeded
							if( p > sum[row+k][col+l])
								sum[row+k][col+l] = p;
						}
						
						if(mode == 10){ // count of class n; condition above breaks if not right class
							sum[row+k][col+l]++;
						}
						
						if(mode == 11){ // if more than one point per cell, last will overwrite
							if(sum[row+k][col+l] != 0)
								count_over++;
							
							sum[row+k][col+l] = point.user_data;	
						}
						
					
					} // end valid distance
					
				} // end within area
			} // for l
		  } // for k
		
	} // end while

	printf("Data read... %d\n", j);
	if(mode == 11)
		printf("Overwritten: %d\n", count_over);
	
	count_no=0;
		
	// write data
	for (i=esri_hdr.nrows-1; i>=0; i--) {
		for (j=0; j<esri_hdr.ncols; j++) {
			if(mode >=6){
				if(mode == 6 || mode == 7)
					avg = sum[i][j]/esri_hdr.cellsize/esri_hdr.cellsize; // normalized 
				else
					avg = sum[i][j];

				if(mode == 8 && avg == - esri_hdr.nodata_value){
						avg = esri_hdr.nodata_value;
				}
				if(avg == esri_hdr.nodata_value || avg == 0){
						count_no++; // have to assume max/min not exactly 0
				}
			}
			else{
			  if(sum[i][j] != 0){
				if(mode == 3)
					avg = num[i][j]/grid[i][j];
				
				else
					avg = grid[i][j]/sum[i][j];
			  }
			  
			  else{
				avg = -9999.0;
				count_no++;
			  }
			}//not higher modes
			
			fwrite(&avg, sizeof(float), 1, bin_out);
		}
	}
	printf("Data written to file\n");
	printf("No data in %d cells (%0.2f%%)\n", count_no, (float) 100.0*count_no/esri_hdr.nrows/esri_hdr.ncols);
	fprintf(hdr_out, "num_nodata %d\n", count_no);
	fprintf(hdr_out, "per_nodata %0.2f\n", (float) 100.0*count_no/esri_hdr.nrows/esri_hdr.ncols);
	
	free(sum);
	if(mode == 6)
		free(lines);
	else{
		if(mode != 8 && mode != 9){
			free(grid);
			free(num);
		}
	}
	// close files
	fclose(file_in);
	fclose(bin_out);
	fclose(hdr_out);

	return 0;
}
