#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include "LAS_structures.h"
#include "read_las.h"

/**
 *  Assumes points sorted by elevation. 
 *  First pass reclassifies (to 1) any points that are not surrounded by 50% + same class
 *  Second pass... finds a potential vegeation points (15) and looks for others nearby
 *  Since only those with at least 50% other veg are still 15, consider it true veg and 
 *  reclassify as 5.
 *
 **/


int main (int argc, const char * argv[]) {
	FILE *file_m1, *file_out, *file_counts;
	FILE *temp1, *temp2;
	FILE *record;

	
	Header1 las_head1; // system info etc
	Header2 las_head2; // point transformation info
	Rec_head rec_head; // point record info
	Point1 p_m1, p_m0;	
	
	int class_old[22]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};	
	int class_new[22]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};	
	int class_fin[22]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};	
	int count_set[3]={0,0,0};
	
	unsigned char uchar;
	int i,j, k, m,n; 
	int pm_count, pt_count; // count of matches and total neighbours
	
	double refE, refN, refZ; // reference point coordinates
	long int capZ_high, capZ_low, capX_high, capX_low, capY_high, capY_low;
	double delta_d;

	float radius;
	int layers, rows, cols;
	int l1, l2, r1, r2, c1, c2, start, end;
	int l, r, c;
	int ***counts;
	int ***accum;
	
	float temp, height, length, width;
	
	unsigned char ret = 0x07, num = 0x38; // identify bits for return number and number of returns
	unsigned char rn, nr; // point return number (from) number of returns 
	
	
	struct tm *ptr;
	time_t tm;
	tm = time(NULL);
	ptr = localtime(&tm);
	
	
	
	// input file: las; 
	if((argc !=4)){
		printf("usage %s  <input m1> <output> <sort file>\n", argv[0]);
		printf("sort file contains counts of points within layer / row / column\n");
		return 0;
	}
	
	
	record = fopen("/Users/km.amolins/code/growpatch_log.txt","a");
	fprintf(record, asctime(ptr));
	fprintf(record, "%s %s %s %s",argv[0], argv[1], argv[2], argv[3]); // function
	fprintf(record, "\n");
	
	// open LAS files
	file_m1 = fopen(argv[1], "rb"); 
	if(file_m1 == NULL){printf("unable to open file %s\n", argv[1]); return 1; }
	file_out = fopen(argv[2], "wb"); 
	if(file_out == NULL){printf("unable to open file %s\n", argv[2]); return 1; }
	
	
	printf("Files open.\n%s\n%s\n%s\n",argv[1],argv[2]);
	
	
	// read LAS headers
	fread(&las_head1, sizeof(Header1),1, file_m1);
	read_recHead(file_m1, &rec_head);
	fread(&las_head2, sizeof(Header2),1, file_m1);
	print_lasHead(las_head1, las_head2, rec_head);
	
	fwrite(&las_head1, sizeof(Header1), 1, file_out);
	write_recHead(file_out, &rec_head);
	fwrite(&las_head2, sizeof(Header2), 1, file_out);
	for(i=las_head1.headsize; i<las_head1.offset; i++){
		fread(&uchar, sizeof(unsigned char), 1, file_m1);
		fwrite(&uchar, sizeof(unsigned char), 1, file_out);                           
	}
	printf("\n");
	
	// open voxel counts
	file_counts = fopen(argv[3], "r");
	if(file_counts == NULL){printf("unable to open file %s\n",argv[5]); return 1;}
	printf("Counts in %s\n", argv[3]);
	
	fscanf(file_counts, "%d\t%f\t%d\t%f\t%d\t%f\n",&layers,&height,&rows,&length,&cols,&width);
	printf("%d layers, %d rows, %d columns\n",layers, rows, cols);
	printf("height %f, length %f, width %f\n",height, length, width);
	
	// allocate memory
	counts = (int ***) calloc(layers, sizeof(int **));
	if(!(counts)){printf("calloc error - layers\n"); return 1;}
	for(i=0; i<layers; i++){
		counts[i] = (int **) calloc(rows, sizeof(int *));
		if(!(counts[i])){printf("calloc error - layer %d", i); return 1;}
		for(j=0; j<cols; j++){
			counts[i][j] = (int *) calloc(cols, sizeof(int));
			if(!(counts[i][j])){printf("calloc error - layer %d row %d", i,j); return 1;}
		} 
	}
	
	accum = (int ***) calloc(layers, sizeof(int **));
	if(!(accum)){printf("calloc error - layers\n"); return 1;}
	for(i=0; i<layers; i++){
		accum[i] = (int **) calloc(rows, sizeof(int *));
		if(!(accum[i])){printf("calloc error - layer %d", i); return 1;}
		for(j=0; j<cols; j++){
			accum[i][j] = (int *) calloc(cols, sizeof(int));
			if(!(accum[i][j])){printf("calloc error - layer %d row %d", i,j); return 1;}
		} 
	}
	
	
	for(i=0; i<layers; i++){
		fscanf(file_counts, "%f\n", &temp);
		for(j=0; j<rows; j++){
			fscanf(file_counts,"\t%f\n", &temp);
			for(k=0; k<cols; k++){
				fscanf(file_counts, "\t\t%f\t%d\t%d\n", &temp, &accum[i][j][k], &counts[i][j][k]);
				
			}
			
		}
	}
	fclose(file_counts);
	printf("Counts read in.\n");

	radius = 2.0;
	printf("Valid distance %f\n", radius);
	
	temp1 = fopen("temp1_gp.pts", "wb");
	if(temp1 == NULL){printf("unable to open file temp1_gp.pts for writing\n"); return 1; }
	temp2 = fopen("temp2_gp.pts", "wb");
	if(temp2 == NULL){printf("unable to open file temp2_gp.pts for writing\n"); return 1; }

	i=0;
	j=0;
	k=0;
	m=0;
	// initial 
	fseek(file_m1, las_head1.offset,0);
	while(i< rec_head.num_points){
		m = las_head1.offset + i * rec_head.rec_len;
		
		fseek(file_m1, m, 0);
		fread(&p_m1, rec_head.rec_len,1, file_m1);
		i++;
		k=0;

		pm_count = -1;
		pt_count = 0;
		// determine columns, rows, layers containing 2 m cube around current point
		capX_high = (long int) p_m1.x + radius / las_head2.x_scale; // unscaled limits
		capX_low = (long int) p_m1.x - radius / las_head2.x_scale;
		capY_high = (long int) p_m1.y + radius / las_head2.y_scale; 
		capY_low = (long int) p_m1.y - radius / las_head2.y_scale;
		capZ_high = (long int) p_m1.z + radius / las_head2.z_scale;
		capZ_low = (long int) p_m1.z - radius / las_head2.z_scale;
		capZ_high = (long int) p_m1.z + radius / las_head2.z_scale; // unscaled limits; no more than 2 m above/below
		capZ_low = (long int) p_m1.z - radius / las_head2.z_scale;
		
		refE = 1.0 * p_m1.x * las_head2.x_scale; // scaled coordinates
		refN = 1.0 * p_m1.y * las_head2.y_scale;
		refZ = 1.0 * p_m1.z * las_head2.z_scale;
		
		c1 = (int) floor((refE + las_head2.x_offset - 3 - las_head2.x_min)/width);
		r1 = (int) floor((refN + las_head2.y_offset - 3 - las_head2.y_min)/length);
		l1 = (int) floor((refZ + las_head2.z_offset - 3 - las_head2.z_min)/height);
		c2 = (int) floor((refE + las_head2.x_offset + 3 - las_head2.x_min)/width);
		r2 = (int) floor((refN + las_head2.y_offset + 3 - las_head2.y_min)/length);
		l2 = (int) floor((refZ + las_head2.z_offset + 3 - las_head2.z_min)/height);
		
		// check within bounds
		if(c1<0)
			c1=0;
		if(r1<0)
			r1=0;
		if(l1<0)
			l1=0;
		
		if(c2>=cols)
			c2=c1;
		if(r2>=rows)
			r2=r1;
		if(l2>=layers)
			l2=l1;
			
		
		for(l = l1; l<=l2; l++){
			//printf("layer %d\t", l);
			for(r = r1; r<=r2; r++){
				//printf("row %d\t", r);
				for(c = c1; c<=c2; c++){
					//printf("col %d\n", c);
					start = accum[l][r][c]; 
					end = accum[l][r][c] + counts[l][r][c];
					// use point accumulations and counts to mark where to start/end looking in low_prob for neighbouring points
					// repeat for each column, row, layer for contained cube
					
					j=start; 
					n = las_head1.offset + j * rec_head.rec_len;
					
					//return 1;
					
					fseek(file_m1, n, 0); // jump to starting point
					while(j < end){
						
						fread(&p_m0, rec_head.rec_len,1, file_m1);
						j++;
						if(i - 1 == j - 1)
							continue;
						
						
						if(p_m0.z > capZ_high || p_m0.z < capZ_low)
							continue; // more than radius above
						
						if(p_m0.y > capY_high || p_m0.y < capY_low)
							continue; // more than radius away in Northing
						
						if(p_m0.x > capX_high || p_m0.x < capX_low)
							continue; // more than radius away in Easting
						//printf("%d, %d, %d\t", p_m0.x, p_m0.y, p_m0.z);
						
						// any reference points at this stage are in (radius) cube 
						delta_d = sqrt(pow(p_m1.z - p_m0.z,2)*las_head2.z_scale*las_head2.z_scale + pow(p_m1.y - p_m0.y,2)*las_head2.y_scale*las_head2.y_scale + pow(p_m1.x - p_m0.x,2)*las_head2.x_scale*las_head2.x_scale);
						
						if(delta_d < radius){
							k++;
							pt_count++; // point in neighbourhood
							if(p_m1.p_class == p_m0.p_class)
								pm_count++; // same class
						}
						
					} // end while j
						
				} // end c
			} // end r
		} // end l
		
		if(pt_count == 0)
			p_m1.user_data = 0;
		else
			p_m1.user_data = (char) floor(1.0*(pm_count + 1)/pt_count * 255.0);
		
		class_old[p_m1.p_class]++;
		if(p_m1.user_data < 255/2 && (p_m1.p_class != 7 && p_m1.p_class != 18))
			p_m1.p_class = 1; // less than 50 % agreement
		class_new[p_m1.p_class]++;
		
		fwrite(&p_m1, rec_head.rec_len, 1, temp1); // write points with new user data
		fwrite(&p_m1, rec_head.rec_len, 1, temp2); // write points with new user data
	}	// end while i	
	
	printf("Comparison complete.\n");
	
	printf("\nOld classes: ");
	fprintf(record, "\nOld classes: ");
	for(i=0; i<19; i++){
		printf("%d %d; ",i, class_old[i]);
		fprintf(record,"%d %d; ",i, class_old[i]);
	}
	printf("\n");
	printf("\nNew classes: ");
	fprintf(record, "\nNew classes: ");
	for(i=0; i<19; i++){
		printf("%d %d; ",i, class_new[i]);
		fprintf(record,"%d %d; ",i, class_new[i]);
	}
	printf("\n");
	fprintf(record, "\n");
	
	fclose(file_m1);
	fclose(temp1);
	fclose(temp2);
	
	
 	fprintf(record, "Phase 1: ");
	tm = time(NULL);
	ptr = localtime(&tm);
 	fprintf(record, asctime(ptr));
	fprintf(record, "\n\n");
	
		   
	// open LAS files
	temp1 = fopen("temp1_gp.pts", "rb");
	if(temp1 == NULL){printf("unable to open file temp1_gp.pts for reading\n"); return 1; }
	temp2 = fopen("temp2_gp.pts", "rb+");
	if(temp2 == NULL){printf("unable to open file temp2_gp.pts for reaad/write\n"); return 1; }

	radius = 1.0;
	printf("Valid distance %f\n", radius);

	
	i=0;
	j=0;
	k=0;
	m=0;
	
	pm_count = 0;
	pt_count = 0;
	while(i< rec_head.num_points){
		m = i * rec_head.rec_len;
		
		fseek(temp1, m, 0);
		fread(&p_m1, rec_head.rec_len,1, temp1);
		i++;
		
		if(p_m1.p_class != 15){ // first grow vegetation
			continue;
		}
		
		for(k=0; k<3; k++)
			count_set[k]=0; // reset 
		
		pm_count++;
		// determine columns, rows, layers containing 2 m cube around current point
		capX_high = (long int) p_m1.x + radius / las_head2.x_scale; // unscaled limits
		capX_low = (long int) p_m1.x - radius / las_head2.x_scale;
		capY_high = (long int) p_m1.y + radius / las_head2.y_scale; 
		capY_low = (long int) p_m1.y - radius / las_head2.y_scale;
		capZ_high = (long int) p_m1.z + radius / las_head2.z_scale;
		capZ_low = (long int) p_m1.z - radius / las_head2.z_scale;
		capZ_high = (long int) p_m1.z + radius / las_head2.z_scale; // unscaled limits; no more than 2 m above/below
		capZ_low = (long int) p_m1.z - radius / las_head2.z_scale;
			
		refE = 1.0 * p_m1.x * las_head2.x_scale; // scaled coordinates
		refN = 1.0 * p_m1.y * las_head2.y_scale;
		refZ = 1.0 * p_m1.z * las_head2.z_scale;
		
		c1 = (int) floor((refE + las_head2.x_offset - 3 - las_head2.x_min)/width);
		r1 = (int) floor((refN + las_head2.y_offset - 3 - las_head2.y_min)/length);
		l1 = (int) floor((refZ + las_head2.z_offset - 3 - las_head2.z_min)/height);
		c2 = (int) floor((refE + las_head2.x_offset + 3 - las_head2.x_min)/width);
		r2 = (int) floor((refN + las_head2.y_offset + 3 - las_head2.y_min)/length);
		l2 = (int) floor((refZ + las_head2.z_offset + 3 - las_head2.z_min)/height);
		
		// check within bounds
		if(c1<0)
			c1=0;
		if(r1<0)
			r1=0;
		if(l1<0)
			l1=0;
		
		if(c2>=cols)
			c2=c1;
		if(r2>=rows)
			r2=r1;
		if(l2>=layers)
			l2=l1;		
		
		for(l = l1; l<=l2; l++){
			for(r = r1; r<=r2; r++){
				for(c = c1; c<=c2; c++){
					start = accum[l][r][c]; 
					end = accum[l][r][c] + counts[l][r][c];
					// use point accumulations and counts to mark where to start/end looking in low_prob for neighbouring points
					// repeat for each column, row, layer for contained cube
					
					j=start; 
					n = j * rec_head.rec_len;
					
					fseek(temp2, n, 0); // jump to starting point
					while(j < end){
						
						fread(&p_m0, rec_head.rec_len,1, temp2);
						j++;
						if(i - 1 == j - 1)
							continue;
						
						if(p_m0.z > capZ_high || p_m0.z < capZ_low)
							continue; // more than radius above
						
						if(p_m0.y > capY_high || p_m0.y < capY_low)
							continue; // more than radius away in Northing
						
						if(p_m0.x > capX_high || p_m0.x < capX_low)
							continue; // more than radius away in Easting
						
						// any reference points at this stage are in (radius) cube 
						delta_d = sqrt(pow(p_m1.z - p_m0.z,2)*las_head2.z_scale*las_head2.z_scale + pow(p_m1.y - p_m0.y,2)*las_head2.y_scale*las_head2.y_scale + pow(p_m1.x - p_m0.x,2)*las_head2.x_scale*las_head2.x_scale);
						
						if(delta_d < radius){
							
							if(p_m0.p_class == 15){
								// add more conditions?
								p_m0.p_class = 5; 
								fseek(temp2, -(rec_head.rec_len), SEEK_CUR);
								fwrite(&p_m0, rec_head.rec_len, 1, temp2);
								pt_count++; // count changed
							}
						
							
						}
						
					} // end while j
					
				} // end c
			} // end r
		} // end l
		
	}	// end while i	
	
	printf("Comparison complete.\n");
	printf("%d points changed\n", pt_count);
	
	fseek(temp2, 0, 0);
	i=0;
	while(i< rec_head.num_points){
		fread(&p_m0, rec_head.rec_len,1, temp2);
		i++;
		class_fin[p_m0.p_class]++;
		fwrite(&p_m0, rec_head.rec_len, 1, file_out);
		
	}
	
	
	printf("\nFinal classes: ");
	fprintf(record, "\nFinal classes: ");
	for(i=0; i<19; i++){
		printf("%d %d; ",i, class_fin[i]);
		fprintf(record,"%d %d; ",i, class_fin[i]);
	}
	printf("\n");
	fprintf(record, "\n");
	
	
	fclose(temp1);
	fclose(temp2);
	fclose(file_out);
	
	tm = time(NULL);
	ptr = localtime(&tm);
 	fprintf(record, asctime(ptr));
	fprintf(record, "\n--- END ---\n");
	fclose(record);
	
	
	return 0;
}
