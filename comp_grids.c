#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include "esri_hdr.h"


/**
 * Compares values in six grids. 
 * Counts how many of the six have data. 
 * For cells where four or more grids have data, calculates and outputs standard deviation.
 * For cells where four or more grids have data and standard deviation less than 0.15, outputs average.
 * Optional, use non-nodata pixels in LAST file to override count; standard deviation criteria still applies.
 **/

int main (int argc, char * argv[]) {
    FILE *file_c1, *file_c2, *file_c3, *file_c4, *file_c5, *file_c6;
	FILE *hdr_in, *count_hdr, *count_bin, *avg_hdr, *avg_bin, *st_hdr, *st_bin;
	FILE *record;
	float **sum, **st_dev, **avg;
	int **counts;
	
	ESRI_hdr esri_hdr; // grid file header
	char header[100], binary[100]; // file names
	char ext1[]=".hdr", ext2[]=".bil"; // file extensions
	
	int i,j,k;
	float value[6];
	int override, grounded;
	
	struct tm *ptr;
	time_t tm;
	tm = time(NULL);
	ptr = localtime(&tm);
	
	
	if(argc != 2){
		printf("usage: %s <last override>\n", argv[0]);
		printf("last override = 1 if override count criteria for non-nodata pixels in last input file\n");
		printf("prompted first for counts, average, standard deviation files then six inputs\n");
		return 0;
	}
	
	record = fopen("/Users/km.amolins/code/compgrids_log.txt","a");
	fprintf(record, asctime(ptr));
	fprintf(record, "%s",argv[0]); // function
	
	override = atoi(argv[1]);
	if(override){
		printf("Override of counts for non-zero pixels\n");
		fprintf(record, " override\n");
	}
	else{
		printf("Count criteria in effect\n");
		fprintf(record, " count all\n");

	}
	
	
	// output file - count of how many grids have ground points in cell
	printf("Output counts: ");
	scanf("%s", &binary);
	strcpy(header, binary);
	strcat(header, ext1);
	count_hdr = fopen(header, "w");
	
	if(count_hdr == NULL){printf("unable to open file %s\n",header); return 1;}
	printf("Successfully opened file %s\n", header);
	
	fprintf(record, "%s ", binary);
	
	strcat(binary, ext2);
	count_bin = fopen(binary, "wb");
	if(count_bin == NULL){printf("unable to open file %s\n",binary); return 1;}
	printf("Successfully opened file %s\n", binary);

	// output file - average value in cell
	printf("Output averages: ");
	scanf("%s", &binary);
	strcpy(header, binary);
	strcat(header, ext1);
	avg_hdr = fopen(header, "w");
	
	if(avg_hdr == NULL){printf("unable to open file %s\n",header); return 1;}
	printf("Successfully opened file %s\n", header);
	
	fprintf(record, "%s ", binary);
	
	strcat(binary, ext2);
	avg_bin = fopen(binary, "wb");
	if(avg_bin == NULL){printf("unable to open file %s\n",binary); return 1;}
	printf("Successfully opened file %s\n", binary);

	// output file - standard deviation of elevation value in cell
	printf("Output standard deviation: ");
	scanf("%s", &binary);
	strcpy(header, binary);
	strcat(header, ext1);
	st_hdr = fopen(header, "w");
	
	if(st_hdr == NULL){printf("unable to open file %s\n",header); return 1;}
	printf("Successfully opened file %s\n", header);
	
	fprintf(record, "%s\n", binary);
	
	strcat(binary, ext2);
	st_bin = fopen(binary, "wb");
	if(st_bin == NULL){printf("unable to open file %s\n",binary); return 1;}
	printf("Successfully opened file %s\n", binary);

	
	
	// open files for comparison
	printf("First file (and reference header): ");
	scanf("%s", &binary);
	
	// open ESRI header file
	strcpy(header, binary);
	strcat(header, ext1);
	hdr_in = fopen(header, "r");
	
	if(hdr_in == NULL){printf("unable to open file %s\n",header); return 1;}
	printf("Successfully opened file %s\n", header);
	
	fprintf(record, "%s\n", binary);
	
	// read header
	read_esri_hdr(hdr_in, &esri_hdr);
	print_esri_hdr(esri_hdr);
	// write ESRI header and close file
	write_esri_hdr(avg_hdr, &esri_hdr);
	write_esri_int(count_hdr, &esri_hdr);
	write_esri_hdr(st_hdr, &esri_hdr);
	fclose(hdr_in);
	fclose(avg_hdr);
	fclose(count_hdr);
	
	// allocate memory
	sum = (float **) calloc(esri_hdr.nrows, sizeof(float *));
	if(!(sum)){printf("calloc error - rows\n"); return 1;}
	for(i=0; i<esri_hdr.nrows; i++){
		sum[i] = (float *) calloc(esri_hdr.ncols, sizeof(float));
		if(!(sum[i])){printf("calloc error - row %d",i); return 1;}
	}
	avg = (float **) calloc(esri_hdr.nrows, sizeof(float *));
	if(!(avg)){printf("calloc error - rows\n"); return 1;}
	for(i=0; i<esri_hdr.nrows; i++){
		avg[i] = (float *) calloc(esri_hdr.ncols, sizeof(float));
		if(!(avg[i])){printf("calloc error - row %d",i); return 1;}
	}
	st_dev = (float **) calloc(esri_hdr.nrows, sizeof(float *));
	if(!(st_dev)){printf("calloc error - rows\n"); return 1;}
	for(i=0; i<esri_hdr.nrows; i++){
		st_dev[i] = (float *) calloc(esri_hdr.ncols, sizeof(float));
		if(!(st_dev[i])){printf("calloc error - row %d",i); return 1;}
	}
	counts = (int **) calloc(esri_hdr.nrows, sizeof(int *));
	if(!(counts)){printf("calloc error - rows\n"); return 1;}
	for(i=0; i<esri_hdr.nrows; i++){
		counts[i] = (int *) calloc(esri_hdr.ncols, sizeof(int));
		if(!(counts[i])){printf("calloc error - row %d",i); return 1;}
	}
	printf("memory allocated\n");
	
	// read in first file
	strcat(binary, ext2);
	file_c1 = fopen(binary, "rb");
	if(file_c1 == NULL){printf("unable to open file %s\n",binary); return 1;}
	printf("Successfully opened file %s\n", binary);
	
	
	// second comparison
	printf("Second file: ");
	scanf("%s", &binary);
	fprintf(record, "%s\n", binary);
	strcat(binary, ext2);
	file_c2 = fopen(binary, "rb");
	if(file_c2 == NULL){printf("unable to open file %s\n",binary); return 1;}
	printf("Successfully opened file %s\n", binary);
	
	// third comparison
	printf("Third file: ");
	scanf("%s", &binary);
	fprintf(record, "%s\n", binary);
	strcat(binary, ext2);
	file_c3 = fopen(binary, "rb");
	if(file_c3 == NULL){printf("unable to open file %s\n",binary); return 1;}
	printf("Successfully opened file %s\n", binary);
	
	// fourth comparison
	printf("Fourth file: ");
	scanf("%s", &binary);
	fprintf(record, "%s\n", binary);
	strcat(binary, ext2);
	file_c4 = fopen(binary, "rb");
	if(file_c4 == NULL){printf("unable to open file %s\n",binary); return 1;}
	printf("Successfully opened file %s\n", binary);
	
	// fifth comparison
	printf("Fifth file: ");
	scanf("%s", &binary);
	fprintf(record, "%s\n", binary);
	strcat(binary, ext2);
	file_c5 = fopen(binary, "rb");
	if(file_c5 == NULL){printf("unable to open file %s\n",binary); return 1;}
	printf("Successfully opened file %s\n", binary);
	
	
	// sixth comparison
	printf("Sixth (last) file: ");
	scanf("%s", &binary);
	fprintf(record, "%s\n", binary);
	strcat(binary, ext2);
	file_c6 = fopen(binary, "rb");
	if(file_c6 == NULL){printf("unable to open file %s\n",binary); return 1;}
	printf("Successfully opened file %s\n", binary);
	
	
	for (i=esri_hdr.nrows-1; i>=0; i--) {
		for (j=0; j<esri_hdr.ncols; j++) {
			fread(&value[0], sizeof(float), 1, file_c1);
			fread(&value[1], sizeof(float), 1, file_c2);
			fread(&value[2], sizeof(float), 1, file_c3);
			fread(&value[3], sizeof(float), 1, file_c4);
			fread(&value[4], sizeof(float), 1, file_c5);
			fread(&value[5], sizeof(float), 1, file_c6);
			
			for(k = 0; k<6; k++){
				// count and sum all values
				if(value[k] != esri_hdr.nodata_value ){
					counts[i][j]++;
					sum[i][j] += value[k];
				}
			}
			
			
			if(counts[i][j] !=0){
				// calculate average then standard deviation
				avg[i][j] = sum[i][j] / counts[i][j];
				for(k = 0; k<6; k++){
					// count and sum all values
					if(value[k] != esri_hdr.nodata_value){
						st_dev[i][j] += pow(value[k] - avg[i][j], 2)/counts[i][j] ;
					}
				}
				if(override && value[5] !=esri_hdr.nodata_value)
					counts[i][j] = 7; // flag as an override pixel
				
			} // at least one ground
			else {
				// no data
				st_dev[i][j] = esri_hdr.nodata_value;
				avg[i][j] = esri_hdr.nodata_value;
			}
			
		} // end cols
	} // end rows
	
	

	// close input files
	fclose(file_c1);
	fclose(file_c2);
	fclose(file_c3);
	fclose(file_c4);
	fclose(file_c5);	
	fclose(file_c6);
	
	
	// write data
	grounded = 0;
	for (i=esri_hdr.nrows-1; i>=0; i--) {
		for (j=0; j<esri_hdr.ncols; j++) {
			fwrite(&counts[i][j], sizeof(int), 1, count_bin);
			
			if(counts[i][j] > 2 && st_dev[i][j] < 0.6)
				grounded++;
			else
				avg[i][j] = esri_hdr.nodata_value;
			fwrite(&avg[i][j], sizeof(float), 1, avg_bin);
			
			fwrite(&st_dev[i][j], sizeof(float), 1, st_bin);
		}
	}
	
	printf("Comparison results: %d out of %0.0f ", grounded, esri_hdr.ncols * esri_hdr.nrows);
	printf("(%0.2f%%) matched pixels.\n", 100.0 * grounded/esri_hdr.ncols/esri_hdr.nrows);
	fprintf(record, "%d out of %0.0f\n\n", grounded, esri_hdr.ncols * esri_hdr.nrows);
	
	fclose(count_bin);
	fclose(avg_bin);
	
	free(counts);
	free(sum);
	free(avg);
	free(st_dev);
	
	return 0;
}
