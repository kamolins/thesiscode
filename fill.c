#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "esri_hdr.h"

/****************************
 Fills gaps in grid (ESRI binary format).
 Currently, calculated value placed in original grid and used for adjacent 
 empty cells.
 Must have at least three neighbouring cells with values.
 
 ****************************/

int main (int argc, const char * argv[]) {
	FILE *hdr_in, *bin_in, *hdr_out, *bin_out;
	ESRI_hdr esri_hdr;
	char header[100], binary[100]; // file names
	char ext1[]=".hdr", ext2[]=".bil"; // file extensios
	float **grid, **hold; // data
	int i, j, k,l;
	int iter, count, changed, neighbour, method;
	float sum;
	int num_iters = 200;

	
	// command line: (name) (input file) (output file)
	if(argc !=4 && argc !=5){
		printf("usage: %s <input> <output> <method> [<iterations>]\n0=avg, 1=min, 2=max\n", argv[0]);
		printf("files are ESRI binary rasters, omit extension\n");
		return 0;
	}

	//open header files
	strcpy(header, argv[1]);
	strcat(header, ext1);
	hdr_in = fopen(header, "r");
	if(hdr_in == NULL){printf("unable to open file %s\n",header); return 1;}
	printf("Successfully opened file %s\n", header);
	
	strcpy(header, argv[2]);
	strcat(header, ext1);
	hdr_out = fopen(header, "w");
	if(hdr_out == NULL){printf("unable to open file %s\n",header); return 1;}
	printf("Successfully opened file %s\n", header);
	
	// read header
	read_esri_hdr(hdr_in, &esri_hdr);
	print_esri_hdr(esri_hdr);
	
	// write esri header
	write_esri_hdr(hdr_out, &esri_hdr);
	
	// close header files
	fclose(hdr_in);
	fclose(hdr_out);
	
	//open binary files	
	strcpy(binary, argv[1]);
	strcat(binary, ext2);
	bin_in = fopen(binary, "rb");
	if(bin_in == NULL){printf("unable to open file %s\n",binary); return 1;}
	printf("Successfully opened file %s\n", binary);
	
	strcpy(binary, argv[2]);
	strcat(binary, ext2);
	bin_out = fopen(binary, "wb");
	if(bin_out == NULL){printf("unable to open file %s\n",binary); return 1;}	
	printf("Successfully opened file %s\n", binary);
	
	// allocate memory
	grid = (float **) calloc(esri_hdr.nrows, sizeof(float *));
	if(!(grid)){printf("calloc error - rows\n"); return 1;}
	for(i=0; i<esri_hdr.nrows; i++){
		grid[i] = (float *) calloc(esri_hdr.ncols, sizeof(float));
		if(!(grid[i])){printf("calloc error - row %d",i); return 1;}
	}
	printf("Allocated memory %0.0f x %0.0f\n", esri_hdr.nrows, esri_hdr.ncols);

	// allocate memory
	hold = (float **) calloc(esri_hdr.nrows, sizeof(float *));
	if(!(hold)){printf("calloc error - rows\n"); return 1;}
	for(i=0; i<esri_hdr.nrows; i++){
		hold[i] = (float *) calloc(esri_hdr.ncols, sizeof(float));
		if(!(hold[i])){printf("calloc error - row %d",i); return 1;}
	}
	printf("Allocated memory %0.0f x %0.0f\n", esri_hdr.nrows, esri_hdr.ncols);
	
	// read data into grid
	for(i=0; i<esri_hdr.nrows; i++){
		for(j=0; j<esri_hdr.ncols; j++){
			fread(&grid[i][j], sizeof(float), 1, bin_in);
			hold[i][j] = esri_hdr.nodata_value;
		}
	}
	printf("Data read\n");
	
	method = atoi(argv[3]);
	printf("Method ");
	if(method==0)
		printf("average\n");
	if(method==1)
		printf("minimum\n");
	if(method==2)
		printf("maximum\n");
	if(method>2 || method<0){
		printf("invalid method");
		return 1;
	}
	
	if(argc == 5)
		num_iters = atoi(argv[4]);
	printf("maximum %d iterations\n", num_iters);
	
	// fill gaps
	count = esri_hdr.nrows*esri_hdr.ncols; // set so at least one pass
	changed = esri_hdr.nrows*esri_hdr.ncols; // set so at least one pass
	iter = 0;
	printf("change threshold %.0f\n",esri_hdr.nrows*esri_hdr.ncols/100/100);
	//while(iter<10){
	while(count>0 && (changed > esri_hdr.nrows*esri_hdr.ncols/100/100 || iter < num_iters)){
		iter++;
		printf("iteration %d\t", iter);
		count=0;
		changed=0;
		for(i=0; i<esri_hdr.nrows; i++){
			for(j=0; j<esri_hdr.ncols; j++){
				if(grid[i][j] == esri_hdr.nodata_value){
					count++; // counter for cells with missing data
					neighbour = 0; // count adjacent cells with data

					if(method==0)
						sum = 0;
					if(method==1)
						sum = 9999;
					if(method==2)
						sum = -9999;
					
					for(k=-1; k<=1; k++){
						if(i+k<0 || i+k>= esri_hdr.nrows)
							continue; // outside grid bounds
						for(l=-1; l<=1; l++){
							if(j+l<0 || j+l>=esri_hdr.ncols)
								continue; // outside grid bounds

							if(grid[i+k][j+l] != esri_hdr.nodata_value){
								if(method==0){
									//average
									sum = sum+grid[i+k][j+l];
								}
								else {
									if(method==1){
										//minimum
										if(grid[i+k][j+l]<sum)
											sum=grid[i+k][j+l];
									}
									else{
										//maximum
										if(grid[i+k][j+l]>sum)
											sum=grid[i+k][j+l];
									}
								}
								neighbour++;
										
							} // end neighbour cells with data
						} // end l
					} // end k
					
					if(neighbour>2){ // has at least 3 neighbours
						if(method==0)
							hold[i][j] = sum/neighbour;
						else
							hold[i][j] = sum;
						
						changed++;
					} 
				} // end missing data

				else{ // valid data
					hold[i][j]=grid[i][j];
				}
			} // end j
		} // end i
		printf("still missing: %d; changed: %d\n", count, changed);
		for(i=0; i<esri_hdr.nrows; i++){
			for(j=0; j<esri_hdr.ncols; j++){
				grid[i][j]=hold[i][j];
				hold[i][j]=esri_hdr.nodata_value;
			}
		}
	} // end while
	// write data to file
	for(i=0; i<esri_hdr.nrows; i++){
		for(j=0; j<esri_hdr.ncols; j++){
			fwrite(&grid[i][j], sizeof(float), 1, bin_out);
		}
	}
	printf("Data written\n");
	
	
	// close binary files
	fclose(bin_in);
	fclose(bin_out);
	
	
	return 0;
	
}
