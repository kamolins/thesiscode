#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include "LAS_structures.h"
#include "read_las.h"
#include "esri_hdr.h"

/**
 * Calculates the ratio of ground points, vegetation points, and other points in specified window
 **/

int main (int argc, const char * argv[]) {
    FILE *file_in, *hdr_in;
	FILE *hdr_out, *bin_out;
	
	char header[100], binary[100]; // file names
	char ext1[]=".hdr", ext2[]=".bil"; // file extensions
	
	Header1 las_head1; // system info etc
	Header2 las_head2; // point transformation info
	Rec_head rec_head; // point record info
	Point1 point;
	ESRI_hdr esri_hdr; // grid file header
	ESRI_new esri_new; // grid file header
	
	
	double maxE, minE, maxN, minN, maxZ, minZ; // limits

	int **g_pts, **v_pts, **t_pts, max_t=0;
	float ratio;
	int i,j, k, l;
	int row, col, window, w;
	
	// command line; (name) (file in) (file out)...
	if(argc !=5) {
		printf( "usage: %s <input> <output> <ref header> <window>\n", argv[0] );
		printf("where input is LAS file\n");
		printf("where output ESRI binary raster, omit extension; ref header gives info\n");
		printf("window is one-dimensional number of pixels, e.g., input 3 = 3 x 3 window\n");
		
		return 0;
	}
	
	// input las file
	file_in = fopen( argv[1], "rb" );
	if(file_in == NULL){ 
		printf("unable to open file %s\n", argv[1]); 
		system("pause");
		return 1;
	} 
	printf("Successfully opened input file %s\n", argv[1]);
	//return 0;
	
	// system and collection info
	printf("Reading Header1\n");
	fread(&las_head1, sizeof(Header1),1, file_in);
	
	// point record info
	read_recHead(file_in, &rec_head);
	
	// point transformation values (scale, offset) and max/min
	printf("Reading Header2\n");
	fread(&las_head2, sizeof(Header2),1, file_in);

	// determine limits
	maxE = (las_head2.x_max - las_head2.x_offset)/las_head2.x_scale;
	minE = (las_head2.x_min - las_head2.x_offset)/las_head2.x_scale;
	printf("Transformed Eastings %f %f\n", minE, maxE);
	maxN = (las_head2.y_max - las_head2.y_offset)/las_head2.y_scale;
	minN = (las_head2.y_min - las_head2.y_offset)/las_head2.y_scale;
	printf("Transformed Northings %f %f\n", minN, maxN);
	maxZ = (las_head2.z_max - las_head2.z_offset)/las_head2.z_scale;
	minZ = (las_head2.z_min - las_head2.z_offset)/las_head2.z_scale;
	printf("Transformed Elevations %f %f\n\n", minZ, maxZ);	
	
	// open ESRI header file for reference
	strcpy(header, argv[3]);
	strcat(header, ext1);
	hdr_in = fopen(header, "r");
	if(hdr_in == NULL){printf("unable to open file %s\n",header); return 1;}
	printf("Successfully opened file %s\n", header);
	
	// read header
	read_esri_hdr(hdr_in, &esri_hdr);
	print_esri_hdr(esri_hdr);
	
	fclose(hdr_in);
		
	// output
	strcpy(header, argv[2]);
	strcat(header, ext1);
	hdr_out = fopen(header, "w");
	if(hdr_out == NULL){printf("unable to open file %s\n",header); return 1;}
	printf("Successfully opened file %s\n", header);

	write_esri_hdr(hdr_out, &esri_hdr);
	fprintf(hdr_out, "nbands 3\n");
	fclose(hdr_out);
	
	strcpy(binary, argv[2]);
	strcat(binary, ext2);
	bin_out = fopen(binary, "wb");
	if(bin_out == NULL){printf("unable to open file %s\n",binary); return 1;}	
	printf("Successfully opened file %s\n", binary);
	
	
	
	// allocate memory ground count
	g_pts = (int **) calloc(esri_hdr.nrows, sizeof(int *));
	if(!(g_pts)){printf("calloc error - rows\n"); return 1;}
	for(i=0; i<esri_hdr.nrows; i++){
		g_pts[i] = (int *) calloc(esri_hdr.ncols, sizeof(int));
		if(!(g_pts[i])){printf("calloc error - row %d",i); return 1;}
	}
	// allocate memory vegetation count
	v_pts = (int **) calloc(esri_hdr.nrows, sizeof(int *));
	if(!(v_pts)){printf("calloc error - rows\n"); return 1;}
	for(i=0; i<esri_hdr.nrows; i++){
		v_pts[i] = (int *) calloc(esri_hdr.ncols, sizeof(int));
		if(!(v_pts[i])){printf("calloc error - row %d",i); return 1;}
	}
	// allocate memory total count
	t_pts = (int **) calloc(esri_hdr.nrows, sizeof(int *));
	if(!(t_pts)){printf("calloc error - rows\n"); return 1;}
	for(i=0; i<esri_hdr.nrows; i++){
		t_pts[i] = (int *) calloc(esri_hdr.ncols, sizeof(int));
		if(!(t_pts[i])){printf("calloc error - row %d",i); return 1;}
	}
	
	
	window = atoi(argv[4]);
	printf("%d x %d window\n", window, window);
	w = (window-1)/2;
	printf("or, %d cells around centre\n", w);
	
	fseek(file_in, las_head1.offset , 0);          
	j=0;
	
	while(j< rec_head.num_points ){
		fread(&point, rec_head.rec_len,1, file_in);
		
		if(point.p_class == 18 || point.p_class == 7 ){ // skip noise
			j++;
			continue; // will need refinement...
		}
		
		row = (int) floor((point.y -  minN)*las_head2.y_scale/esri_hdr.cellsize);
		col = (int) floor((point.x -  minE)*las_head2.x_scale/esri_hdr.cellsize);

		for(k=-w; k<=w; k++){
			if(row+k < 0 || row+k >= esri_hdr.nrows)
				continue;
			
			for(l=-w; l<=w; l++){
				if(col+l < 0 || col+l >= esri_hdr.ncols)
					continue;
				
				if(point.p_class == 2 || point.p_class == 3){
					g_pts[row+k][col+l]++;
				}
				
				if(point.p_class == 14 || point.p_class == 15) {
					v_pts[row+k][col+l]++;
				}
				t_pts[row+k][col+l]++;
				if(t_pts[row+k][col+l] > max_t)
					max_t = t_pts[row+k][col+l];
				
			} // l window
		} // k window
		//printf("\n");
		
		j++;
	}
	printf("Data read and points counted\n");
	// write data to file
	for(i=esri_hdr.nrows-1; i>=0; i--){
		for(j=0; j<esri_hdr.ncols; j++){
			// ground first (R)
			if(t_pts == 0)
				ratio = esri_hdr.nodata_value;
			else
				ratio =1.0* g_pts[i][j] / t_pts[i][j];
			
			fwrite(&ratio, sizeof(float), 1, bin_out);
		}
		for(j=0; j<esri_hdr.ncols; j++){
			// vegetation second (G)
			if(t_pts == 0)
				ratio = esri_hdr.nodata_value;
			else
				ratio = 1.0* v_pts[i][j] / t_pts[i][j];
			
			fwrite(&ratio, sizeof(float), 1, bin_out);
		}
		for(j=0; j<esri_hdr.ncols; j++){
			// remainder (B)
			if(t_pts == 0)
				ratio = esri_hdr.nodata_value;
			else
				ratio = 1.0* t_pts[i][j]/max_t;
			
			fwrite(&ratio, sizeof(float), 1, bin_out);
		}
		
		
	}
	printf("Data written\n");
	// close binary files
	fclose(bin_out);
	
	free(v_pts);
	free(g_pts);
	
	
	return 0;
}
