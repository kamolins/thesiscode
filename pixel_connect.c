#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include "esri_hdr.h"

/* Starts at lowest value pixels and grows the ground
 * User specified angle threshold	
 */

/* converts integer into string */
char* itoa(unsigned long num) {
	char* retstr = calloc(12, sizeof(char));
	if (sprintf(retstr, "%03ld", num) > 0) {
		return retstr;
	} else {
		return NULL;
	}
}


int main (int argc, const char * argv[]) {
	FILE *hdr_in, *bin_in, *hdr_out, *bin_out; // actual 
	FILE *bin_ref; // reference image
	FILE *record;

	ESRI_hdr esri_hdr; // grid file header

	char header[100], binary[100]; // file names
	char ext1[]=".hdr", ext2[]=".bil"; // file extensions

	float **ref_grid, **actual, **hold, **window;
	int i,j, k,l, m;
	float nd=-9999.0;
	float temp; // voxel code changed to binary 0/1 output, need to read in differently
	float rows, cols;
	int count, touch, flag;
	double run, rise;
	double arad; // angle in radians
	float angle; // user input angle
	
	struct tm *ptr;
	time_t tm;
	tm = time(NULL);
	ptr = localtime(&tm);
	
	
	if(argc != 5){
		printf("usage %s  <input> <output> <reference> <angle>\n", argv[0]);
		printf("where all files are bil format rasters, omit extension\n");
		printf("reference file contains seed pixels\n");
		printf("angle is the allowable slope angle, in decimal degrees, for connected ground\n");
		return 0;
	}
	
	record = fopen("/Users/km.amolins/code/ground_log.txt","a");
	fprintf(record, asctime(ptr));
	fprintf(record, "%s %s %s %s %s\n",argv[0], argv[1], argv[2], argv[3], argv[4]); // function
	
	//open header files
	strcpy(header, argv[1]);
	strcat(header, ext1);
	hdr_in = fopen(header, "r");
	if(hdr_in == NULL){printf("unable to open file %s\n",header); return 1;}
	printf("Successfully opened file %s\n", header);
	
	strcpy(header, argv[2]);
	strcat(header, ext1);
	hdr_out = fopen(header, "w");
	if(hdr_out == NULL){printf("unable to open file %s\n",header); return 1;}
	printf("Successfully opened file %s\n", header);
	
	// read header
	read_esri_hdr(hdr_in, &esri_hdr);
	print_esri_hdr(esri_hdr);
	rows=esri_hdr.nrows;
	cols=esri_hdr.ncols;
	
	// write esri header
	write_esri_hdr(hdr_out, &esri_hdr);
	
	// close header files
	fclose(hdr_in);
	fclose(hdr_out);
	
	//open binary files	
	// input
	strcpy(binary, argv[1]);
	strcat(binary, ext2);
	bin_in = fopen(binary, "rb");
	if(bin_in == NULL){printf("unable to open file %s\n",binary); return 1;}
	printf("Successfully opened file %s\n", binary);

	// output
	strcpy(binary, argv[2]);
	strcat(binary, ext2);
	bin_out = fopen(binary, "wb");
	if(bin_out == NULL){printf("unable to open file %s\n",binary); return 1;}	
	printf("Successfully opened file %s\n", binary);
	
	// reference
	strcpy(binary, argv[3]);
	strcat(binary, ext2);
	bin_ref = fopen(binary, "rb");
	if(bin_ref == NULL){printf("unable to open file %s\n",binary); return 1;}
	printf("Successfully opened file %s\n", binary);
	
	// allocate memory ref_grid
	ref_grid = (float **) calloc(rows, sizeof(float *));
	if(!(ref_grid)){printf("calloc error - rows\n"); return 1;}
	for(i=0; i<rows; i++){
		ref_grid[i] = (float *) calloc(cols, sizeof(float));
		if(!(ref_grid[i])){printf("calloc error - row %d",i); return 1;}
	}
	
	// allocate memory actual elevations
	actual = (float **) calloc(rows, sizeof(float *));
	if(!(actual)){printf("calloc error - rows\n"); return 1;}
	for(i=0; i<rows; i++){
		actual[i] = (float *) calloc(cols, sizeof(float));
		if(!(actual[i])){printf("calloc error - row %d",i); return 1;}
	}
	// allocate memory transferred
	hold = (float **) calloc(rows, sizeof(float *));
	if(!(hold)){printf("calloc error - rows\n"); return 1;}
	for(i=0; i<rows; i++){
		hold[i] = (float *) calloc(cols, sizeof(float));
		if(!(hold[i])){printf("calloc error - row %d",i); return 1;}
	}
	printf("Memory allocated\n");
	
	// create 3x3 window
	window = (float **) calloc(3, sizeof(float *));
	for(i=0; i<3; i++){
		window[i] = (float *) calloc(3, sizeof(float));
	}
	
	// read in, initialize
	for(i=0;i<rows;i++){
		for(j=0;j<cols;j++){
			fread(&temp, sizeof(float), 1, bin_ref);
			if(temp == 1)
				ref_grid[i][j] = temp; // seed
			else
				ref_grid[i][j] = nd; // not seed
			fread(&actual[i][j], sizeof(float), 1, bin_in);
			hold[i][j]=nd;
		}
	} 
	
	printf("Memory allocated\n");
	fclose(bin_ref);
	fclose(bin_in);
	
	count = 0;
	// starting elevations
	for(i=0; i<rows; i++){
		for(j=0; j<cols; j++){
			if(ref_grid[i][j] != nd ){
				hold[i][j] = actual[i][j]; // transfer seed pixel values 
				count++;
			}
			ref_grid[i][j] = nd; // clear grid
		}
	}
	printf("Starting elevations transferred to holding grid %d\n", count);
	
	count = 1;
	m=0;
	
	angle = atof(argv[4]);
	arad = angle * 3.14159 / 180.0; // conversion
	printf("angle threshold = %f degrees = %f radians\n", angle, arad);
	
	while(count>0){
	//while(m<100){
		printf("%d..", m);
		fprintf(record, "pass %d..",m);
		m++;
		// from "top" left, left to right then down
		for(i=rows-1; i>=0; i--){
			for(j=0; j<cols; j++){
			if(hold[i][j] == nd){
				// reset
				touch=0;
				for(k=0; k<3; k++){
					for(l=0; l<3; l++){
						window[k][l]=nd;
					}
				}
				
				for(k=-1; k<=1; k++){
					if(i+k<0 || i+k>=rows)
						continue;
					
					for(l=-1; l<=1; l++){
						if(j+l<0 || j+l >= cols)
							continue;
												
						if(hold[i+k][j+l] != nd){ //adjacent cell flagged as ground
							touch++; 
							window[k+1][l+1]=actual[i+k][j+l]; // place actual cell value in window
						}
					} // end l
				} // end k

				if(touch > 0){ // at least one adjacent cell flagged as ground
			        for(k=0; k<3; k++){ 
						for(l=0; l<3; l++){ // 3x3 window
							if(window[k][l] !=nd){ 
								run = esri_hdr.cellsize * sqrt(pow(k-1,2)+pow(l-1,2)); // xy plane distance
								rise = actual[i][j] - window[k][l]; // elevation difference
								
								if(rise/run < arad){ 
								   	ref_grid[i][j] = actual[i][j];
									flag=1;
								}
							}
						}
					}
					//return 0;
				} // cell without value yet, touching cell(s) with value
				
			} // end no transfer
		} // end j
		} // end i	
		if(flag>0){ 
		// flag means actual cell value transferred to the reference grid
			for(i=0; i<rows; i++){
				for(j=0; j<cols; j++){
					if(ref_grid[i][j] != nd)
						hold[i][j] = ref_grid[i][j]; // new cells flagged as ground
				}
			}
			flag=0;
		} // at least one transfer
		
		// from "top" right, top to bottom then across
		for(j=cols-1; j>0; j--){
			for(i=rows-1; i>=0; i--){
			
			
			if(hold[i][j] == nd){
				// reset
				touch=0;
				for(k=0; k<3; k++){
					for(l=0; l<3; l++){
						window[k][l]=nd;
					}
				}
				
				for(k=-1; k<=1; k++){
					if(i+k<0 || i+k>=rows)
						continue;
					
					
					for(l=-1; l<=1; l++){
						if(j+l<0 || j+l >= cols)
							continue;
						
						if(hold[i+k][j+l] != nd){
							touch++;
							window[k+1][l+1]=actual[i+k][j+l];
						}
					} // end l
				} // end k
				
				if(touch > 0){
			        for(k=0; k<3; k++){
						for(l=0; l<3; l++){
							if(window[k][l] !=nd){
						 		run = sqrt(pow(k-1,2)+pow(l-1,2));
								rise = actual[i][j] - window[k][l];
								if(rise/run < arad){
								   	ref_grid[i][j] = actual[i][j];
									flag=1;
		 						}
							}
						}
					}
				} // cell without value yet, touching cell(s) with value
				
			} // end no transfer
		} // end i
		} // end j	
		if(flag>0){
		for(i=0; i<rows; i++){
			for(j=0; j<cols; j++){
				if(ref_grid[i][j] != nd)
					hold[i][j] = ref_grid[i][j];
			}
		}
		flag=0;
	} // at least one transfer
		
		// from "bottom" right, right to left then up
		for(i=0; i<rows; i++){
			for(j=cols-1; j>0; j--){
			if(hold[i][j] == nd){
				// reset
				touch=0;
				for(k=0; k<3; k++){
					for(l=0; l<3; l++){
						window[k][l]=nd;
					}
				}
				
				for(k=-1; k<=1; k++){
					if(i+k<0 || i+k>=rows)
						continue;
					
					
					for(l=-1; l<=1; l++){
						if(j+l<0 || j+l >= cols)
							continue;
						
						if(hold[i+k][j+l] != nd){
							touch++;
							window[k+1][l+1]=actual[i+k][j+l];
						}
					} // end l
				} // end k
				
				if(touch > 0){
			        for(k=0; k<3; k++){
						for(l=0; l<3; l++){
							if(window[k][l] !=nd){
								run = sqrt(pow(k-1,2)+pow(l-1,2));
			 					rise = actual[i][j] - window[k][l];
								if(rise/run < arad){
								   	ref_grid[i][j] = actual[i][j];
									flag=1;
								}
							}
						}
					}
				} // cell without value yet, touching cell(s) with value
				
			} // end no transfer
		} // end j
		} // end i	
		if(flag>0){
		for(i=0; i<rows; i++){
			for(j=0; j<cols; j++){
				if(ref_grid[i][j] != nd)
					hold[i][j] = ref_grid[i][j];
			}
		}
		flag=0;
	} // at least one transfer
		
		// from "bottom" left, bottom to top then across
		for(j=0; j<cols; j++){
			for(i=0; i<rows; i++){
			
			if(hold[i][j] == nd){
				// reset
				touch=0;
				for(k=0; k<3; k++){
					for(l=0; l<3; l++){
						window[k][l]=nd;
					}
				}
				
				for(k=-1; k<=1; k++){
					if(i+k<0 || i+k>=rows)
						continue;
					
					
					for(l=-1; l<=1; l++){
						if(j+l<0 || j+l >= cols)
							continue;
						
						if(hold[i+k][j+l] != nd){
							touch++;
							window[k+1][l+1]=actual[i+k][j+l];
						}
					} // end l
				} // end k
				
				if(touch > 0){
			        for(k=0; k<3; k++){
						for(l=0; l<3; l++){
							if(window[k][l] !=nd){
								run = sqrt(pow(k-1,2)+pow(l-1,2));
								rise = actual[i][j] - window[k][l];
								if( rise/run <arad){
								   	ref_grid[i][j] = actual[i][j];
									flag=1;
								}
							}
						}
					}
				} // cell without value yet, touching cell(s) with value				
			} // end no transfer
		} // end i
		} // end j
		if(flag>0){
		for(i=0; i<rows; i++){
			for(j=0; j<cols; j++){
				if(ref_grid[i][j] != nd)
					hold[i][j] = ref_grid[i][j];
			}
		}
		flag=0;
	} // at least one transfer
		
		
		count=0; 
		for(i=0;i<rows; i++){
			for(j=0;j<cols; j++){
				if(ref_grid[i][j] != nd){
					count++; // count how many new flagged
					ref_grid[i][j]=nd; // reset new flagged grid
				}
			}
		}
		fprintf(record, "flagged %d\n",count);
		
	} // end while
	
	// write data to file
	for(i=0; i<rows; i++){
		for(j=0; j<cols; j++){
			fwrite(&hold[i][j], sizeof(float), 1, bin_out);
		}
	}
	printf("Data written\n");
	
	free(actual);
	free(hold);
	
	// close binary files
	fclose(bin_out);
	
	tm = time(NULL);
	ptr = localtime(&tm);
 	fprintf(record, asctime(ptr));
	fprintf(record, "\n--- END ---\n");
	fclose(record);
	
    
	return 0;
}
