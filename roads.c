#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include "LAS_structures.h"
#include "read_las.h"
/**
 * Classifies ground points (class 2, 11, 12) as low veg (3) if above
 * specified intensity threshold
 **/

int main (int argc, const char * argv[]) {
	FILE *file_in, *file_out;
	FILE *record;
	
	Header1 las_head1; // system info etc
	Header2 las_head2; // point transformation info
	Rec_head rec_head; // point record info
	Point1 point;	
	
	int classes[22]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};	
	unsigned char uchar;
	int i,j;
	
	int int_cap;
	unsigned char ret = 0x07, num = 0x38; // identify bits for return number and number of returns
	unsigned char rn, nr; // point return number (from) number of returns 

	struct tm *ptr;
	time_t tm;
	tm = time(NULL);
	ptr = localtime(&tm);
	
	
	
	// input file: las; 
	if((argc !=4)){
		printf("usage %s  <input> <output> <threshold>\n", argv[0]);
		printf("intensity threshold integer value; input and output las\n");
		return 0;
	}
	
	
	record = fopen("/Users/km.amolins/code/roads_log.txt","a");
	fprintf(record, asctime(ptr));
	fprintf(record, "%s %s %s %s",argv[0], argv[1], argv[2], argv[3]); // function
	fprintf(record, "\n");
	
	// open LAS files
	file_in = fopen(argv[1], "rb"); 
	if(file_in == NULL){printf("unable to open file %s\n", argv[1]); return 1; }
	file_out = fopen(argv[2], "wb"); 
	if(file_out == NULL){printf("unable to open file %s\n", argv[2]); return 1; }
	
	
	printf("Files open.\n%s\n%s\n",argv[1],argv[2]);
	
	
	// read LAS headers
	fread(&las_head1, sizeof(Header1),1, file_in);
	read_recHead(file_in, &rec_head);
	fread(&las_head2, sizeof(Header2),1, file_in);
	print_lasHead(las_head1, las_head2, rec_head);
	
	fwrite(&las_head1, sizeof(Header1), 1, file_out);
	write_recHead(file_out, &rec_head);
	fwrite(&las_head2, sizeof(Header2), 1, file_out);
	
	for(i=las_head1.headsize; i<las_head1.offset; i++){
		fread(&uchar, sizeof(unsigned char), 1, file_in);
		fwrite(&uchar, sizeof(unsigned char), 1, file_out);                           
	}
	printf("\n");
	
	int_cap = atoi(argv[3]);
	printf("Road/Low Vegetation threshold %d\n", int_cap);
	
	j=0;
	// initial 
	fseek(file_in, las_head1.offset,0);
	
	while(j< rec_head.num_points){
		fread(&point, rec_head.rec_len,1, file_in);
		j++;
		
		rn = point.bits & ret; // find return number
		nr = (point.bits & num) >>3; // find number of returns

		
		if(point.p_class == 2 || point.p_class == 12 || point.p_class ==11){
			if(point.i > int_cap)
				point.p_class = 3; // low vegetation
			else
				point.p_class = 11; // road surface
			
			if(nr != 1)
				point.p_class = 2; // not single returns, intensity diluted

		}
		classes[point.p_class]++;
		fwrite(&point, rec_head.rec_len, 1, file_out); // write points with new user data

	}


	printf("\nClasses: ");
	fprintf(record, "\nClasses: ");
	for(i=0; i<19; i++){
		printf("%d %d; ",i, classes[i]);
		fprintf(record,"%d %d; ",i, classes[i]);
	}
	printf("\n");
	
	fclose(file_in);
	fclose(file_out);
	
	tm = time(NULL);
	ptr = localtime(&tm);
 	fprintf(record, asctime(ptr));
	fprintf(record, "\n--- END ---\n");
	fclose(record);
	
	
	fclose(record);
    
	return 0;
}
