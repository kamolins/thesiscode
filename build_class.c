#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include "LAS_structures.h"
#include "read_las.h"
#include "esri_hdr.h"


/**
 * Classify rooftops as 6
 * Ground must already be classified.
 * Class 16 from veg cleaned up (5x). 
 **/

int main (int argc, const char * argv[]) {
	FILE *file_in, *file_out; // las files
	FILE *b16_bin, *b16_hdr; // ruggedness index
	FILE *ground_hdr, *ground_bin; // ground approximation
	FILE *record;
	
	char header[100], binary[100]; // file names
	char ext1[]=".hdr", ext2[]=".bil"; // file extensions
	
	Header1 las_head1; // system info etc
	Header2 las_head2; // point transformation info
	Rec_head rec_head; // point record info
	Point1 point;
	ESRI_hdr esri_hdr, esri_bm; // grid file header
	ESRI_new esri_new; // grid file header
	
	unsigned char ret = 0x07, num = 0x38, rn, nr; // point return number
	unsigned char uchar;
	int i,j, cut; 
	int grid_type, rows, cols;
	float cellsize, nodata_value;
	int row, col, rR, cR;
	
	int classes[22]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};	
	
	double maxE, minE, maxN, minN, maxZ, minZ; // limits
	float **ground; 
	//int **b16;
	float **b16;
	float elev, hag, bdiff;
	//double px, py;
	
	struct tm *ptr;
	time_t tm;
	tm = time(NULL);
	ptr = localtime(&tm);
	
	// command line; (name) (file in) (file out)...
	if(argc !=6) {
		printf( "usage: %s <input> <output> <ground> <building mask> <grid type>\n", argv[0] );
		printf("where input and output are LAS, ground is ESRI binary rasters - omit extension\n");
		printf("ground bald earth (no objects, filled)\n");
		printf("building mask from generated from class 16 output in veg class, infused with min elevation\n");
		printf("grid type 0 = old, 1 = new\n");

		return 0;
	}
	
	record = fopen("/Users/km.amolins/code/classbuild_log.txt","a");
	fprintf(record, asctime(ptr));
	fprintf(record, "%s %s %s %s %s\n",argv[0], argv[1], argv[2], argv[3], argv[4]); // function
	
	// input las file
	file_in = fopen( argv[1], "rb" );
	if(file_in == NULL){ 
		printf("unable to open file %s\n", argv[1]); 
		system("pause");
		return 1;
	} 
	printf("Successfully opened input file %s\n", argv[1]);
	
	// output las file
	file_out = fopen( argv[2], "wb+" );
	if(file_out == NULL){ 
		printf("unable to open file %s\n", argv[2]); 
		system("pause");
		return 1;
	} 
	printf("Successfully opened output file %s\n", argv[2]);
	
	printf("LAS Files open.\n");
	
	// system and collection info
	printf("Reading Header1\n");
	fread(&las_head1, sizeof(Header1),1, file_in);
	fwrite(&las_head1, sizeof(Header1), 1, file_out);
	
	// point record info
	read_recHead(file_in, &rec_head);
	write_recHead(file_out, &rec_head);
	
	// point transformation values (scale, offset) and max/min
	printf("Reading Header2\n");
	fread(&las_head2, sizeof(Header2),1, file_in);
	fwrite(&las_head2, sizeof(Header2), 1, file_out);
	
	// variable length records
	print_lasHead(las_head1, las_head2, rec_head);
	for(i=las_head1.headsize; i<las_head1.offset; i++){
		fread(&uchar, sizeof(unsigned char), 1, file_in);
		fwrite(&uchar, sizeof(unsigned char), 1, file_out);                           
	}
	printf("\n");
	
	// determine limits
	maxE = (las_head2.x_max - las_head2.x_offset)/las_head2.x_scale;
	minE = (las_head2.x_min - las_head2.x_offset)/las_head2.x_scale;
	printf("Transformed Eastings %f %f\n", minE, maxE);
	maxN = (las_head2.y_max - las_head2.y_offset)/las_head2.y_scale;
	minN = (las_head2.y_min - las_head2.y_offset)/las_head2.y_scale;
	printf("Transformed Northings %f %f\n", minN, maxN);
	maxZ = (las_head2.z_max - las_head2.z_offset)/las_head2.z_scale;
	minZ = (las_head2.z_min - las_head2.z_offset)/las_head2.z_scale;
	printf("Transformed Elevations %f %f\n\n", minZ, maxZ);	
	
	
	// open ground header file
	strcpy(header, argv[3]);
	strcat(header, ext1);
	ground_hdr = fopen(header, "r");
	if(ground_hdr == NULL){printf("unable to open file %s\n",header); return 1;}
	printf("Successfully opened file %s\n", header);
	
	// read header
	read_esri_hdr(ground_hdr, &esri_hdr);
	print_esri_hdr(esri_hdr);
	
	// open building header file
	strcpy(header, argv[4]);
	strcat(header, ext1);
	b16_hdr = fopen(header, "r");
	if(b16_hdr == NULL){printf("unable to open file %s\n",header); return 1;}
	printf("Successfully opened file %s\n", header);
	
	// read header
	grid_type = atoi(argv[5]);
	
	if(grid_type == 1){ // type = new
		read_esri_new(b16_hdr, &esri_new);
		print_esri_new(esri_new);
		rows = esri_new.nrows;
		cols = esri_new.ncols;
		cellsize = esri_new.xdim;
		nodata_value = esri_new.nodata;
		
	}
	if(grid_type == 0){ // type = 0 = old
		read_esri_hdr(b16_hdr, &esri_bm);
		print_esri_hdr(esri_bm);
		rows = esri_bm.nrows;
		cols = esri_bm.ncols;
		cellsize = esri_bm.cellsize;
		nodata_value = esri_bm.nodata_value;
		
	}
	
		
	// close header files
	fclose(ground_hdr);
	fclose(b16_hdr);
	
	
	//open binary files	
	// ground
	strcpy(binary, argv[3]);
	strcat(binary, ext2);
	ground_bin = fopen(binary, "rb");
	if(ground_bin == NULL){printf("unable to open file %s\n",binary); return 1;}
	printf("Successfully opened file %s\n", binary);
	
	// ruggedness
	strcpy(binary, argv[4]);
	strcat(binary, ext2);
	b16_bin = fopen(binary, "rb");
	if(b16_bin == NULL){printf("unable to open file %s\n",binary); return 1;}
	printf("Successfully opened file %s\n", binary);
	
	
	// allocate memory
	ground = (float **) calloc(esri_hdr.nrows, sizeof(float *));
	if(!(ground)){printf("calloc error - rows\n"); return 1;}
	for(i=0; i<esri_hdr.nrows; i++){
		ground[i] = (float *) calloc(esri_hdr.ncols, sizeof(float));
		if(!(ground[i])){printf("calloc error - row %d",i); return 1;}
	}
	printf("Allocated memory ground %0.0f x %0.0f\n", esri_hdr.nrows, esri_hdr.ncols);
	
	b16 = (float **) calloc(rows, sizeof(float *));
	if(!(b16)){printf("calloc error - rows\n"); return 1;}
	for(i=0; i<rows; i++){
		b16[i] = (float *) calloc(cols, sizeof(float));
		if(!(b16[i])){printf("calloc error - row %d",i); return 1;}
	}
	printf("Allocated memory b16 %d x %d\n", rows, cols);
	
	// read in data and close binary files
	for (i=esri_hdr.nrows-1; i>=0; i--) {
		for (j=0; j<esri_hdr.ncols; j++) {
			fread(&ground[i][j], sizeof(float), 1, ground_bin);
		}
	}
	for (i=rows-1; i>=0; i--) {
		for (j=0; j<cols; j++) {
			fread(&b16[i][j], sizeof(float), 1, b16_bin);
		}
	}
	
	
	fclose(ground_bin);
	fclose(b16_bin);
	
	fseek(file_in, las_head1.offset , 0);          
	j=0;
	i=0;
	cut=0;
	while(j< rec_head.num_points && i<10){
		fread(&point, rec_head.rec_len,1, file_in);
		
		rn = point.bits & ret; // find return number
		nr = (point.bits & num) >>3; // find number of returns
		
		row = (int) floor((point.y -  minN)*las_head2.y_scale/esri_hdr.cellsize);
		col = (int) floor((point.x -  minE)*las_head2.x_scale/esri_hdr.cellsize);
		
		rR = (int) floor((point.y -  minN)*las_head2.y_scale/cellsize);
		cR = (int) floor((point.x -  minE)*las_head2.x_scale/cellsize);

		
		if(row<0 || row>=esri_hdr.nrows || col<0 || col>=esri_hdr.ncols){
			printf("out! ");
			fprintf(record, "%d %d\t",point.x, point.y);
			fwrite(&point, rec_head.rec_len,1, file_out);
			classes[point.p_class]++;
			j++;
			cut++;
			if(cut > 100){
				printf("more than 100 points outside. quitting.");
				return 1;
			}
			
			continue;
		}
		
		if(rR<0 || rR>=rows || cR<0 || cR>=cols){
			printf("out! off building");
			fprintf(record, "%d %d\t",point.x, point.y);
			fwrite(&point, rec_head.rec_len,1, file_out);
			classes[point.p_class]++;
			j++;
			cut++;
			if(cut > 100){
				printf("more than 100 points outside. quitting.");
				return 1;
			}
			
			continue;
		}
		
		if(ground[row][col]==esri_hdr.nodata_value){ // no ground reference
			if(b16[rR][cR] > 0){ // no ground, but building
				elev = 1.0 * point.z * las_head2.z_scale + las_head2.z_offset; // real Z value  
				bdiff = elev - b16[row][col];
				
				if(bdiff< 1.0 && elev > 0)
					point.p_class = 6; // building
				else
					point.p_class = 13; // unknown
				
			}
			printf("%d\t%d %d %d %d\n",j,point.x, point.y, point.z, point.p_class);
			fwrite(&point, rec_head.rec_len,1, file_out);
			j++;
			continue;
		}
		
		if(point.p_class != 2 && point.p_class != 7 && point.p_class !=18){ // assume just ground and noise correct
			elev = 1.0 * point.z * las_head2.z_scale + las_head2.z_offset; // real Z value  
			hag = elev - ground[row][col]; // height above ground
			bdiff = elev - b16[rR][cR]; // height above building mask value
			
			if(b16[rR][cR] > 0){
				if(bdiff < 1 && hag > 0.3)
					point.p_class = 6; // building
				else
					point.p_class = 13; // unknown
			}
					} // end check class
		
		
		fwrite(&point, rec_head.rec_len,1, file_out);
		classes[point.p_class]++; // increment appropriate counter
		
		j++;
	} // end while in points		
	
	printf("\nClasses: ");
	fprintf(record, "\nClasses: ");
	for(i=0; i<19; i++){
		printf("%d %d; ",i, classes[i]);
		fprintf(record,"%d %d; ",i, classes[i]);
	}
	printf("\n");
	fprintf(record, "\n\n");
	
	
	free(ground);
	free(b16);
	fclose(file_in);
	fclose(file_out);
	fclose(record);
	
	
	return 0;
}
