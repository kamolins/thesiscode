#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include "LAS_structures.h"
#include "read_las.h"
#include "esri_hdr.h"

/**
 Classify ground ...
 Input reference ground raster, determined through other means.
 Points classified as ground if within 1 m of ground elevation
 Doesn't reclassify points already flagged as ground (or something else).
 
 **/
int main (int argc, const char * argv[]) {
	FILE *file_in, *file_out; // las files
	FILE *group_hdr;
	FILE *ground_bin; // ground approximation
	FILE *record;
	
	char header[100], binary[100]; // file names
	char ext1[]=".hdr", ext2[]=".bil"; // file extensions
	
	Header1 las_head1; // system info etc
	Header2 las_head2; // point transformation info
	Rec_head rec_head; // point record info
	Point1 point;
	ESRI_hdr esri_hdr; // grid file header
	ESRI_new esri_new;
	int grid_type, rows, cols;
	float cellsize, nodata_value;
	
	unsigned char ret = 0x07, num = 0x38, rn, nr; // point return number
	unsigned char uchar;
	int i,j, cut; 
	int row, col;
	int reset, check, split, pull;
	
	int classes[22]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};	
	
	double maxE, minE, maxN, minN, maxZ, minZ; // limits
	float **ground;
	float elev, hag, hold;
	//double px, py;
	
	struct tm *ptr;
	time_t tm;
	tm = time(NULL);
	ptr = localtime(&tm);
	
	
	// command line; (name) (file in) (file out)...
	if(argc !=9) {
		printf( "usage: %s <input> <output> <ground> <grid type> <reset ground?> <check all?> <split?> <pull?>\n", argv[0] );
		printf("where input and output are LAS, ground is ESRI binary rasters - omit extension\n");
		printf("ground bald earth (no objects, filled), grid type 0 = old, 1 = new, 2 = three layers (ground, other, LiDAR)\n");
		printf("flags 0 = no, 1 = yes: reset (set all classes to 0) check (check already classed ground)\n");
		printf("either/or/neither split (class < -1 as 11, >0.3 as 12) pull (points up to 10 m below surface are classed 12)\n");
		return 0;
	}
	
	record = fopen("/Users/km.amolins/code/classground_log.txt","a");
	fprintf(record, asctime(ptr));
	fprintf(record, "%s %s %s %s %s %s\n",argv[0], argv[1], argv[2], argv[3], argv[4], argv[5]); // function

	// input las file
	file_in = fopen( argv[1], "rb" );
	if(file_in == NULL){ 
		printf("unable to open file %s\n", argv[1]); 
		system("pause");
		return 1;
	} 
	printf("Successfully opened input file %s\n", argv[1]);
	
	// output las file
	file_out = fopen( argv[2], "wb+" );
	if(file_out == NULL){ 
		printf("unable to open file %s\n", argv[2]); 
		system("pause");
		return 1;
	} 
	printf("Successfully opened output file %s\n", argv[2]);
	
	printf("LAS Files open.\n");
	
	// system and collection info
	printf("Reading Header1\n");
	fread(&las_head1, sizeof(Header1),1, file_in);
	fwrite(&las_head1, sizeof(Header1), 1, file_out);
	
	// point record info
	read_recHead(file_in, &rec_head);
	write_recHead(file_out, &rec_head);
	
	// point transformation values (scale, offset) and max/min
	printf("Reading Header2\n");
	fread(&las_head2, sizeof(Header2),1, file_in);
	fwrite(&las_head2, sizeof(Header2), 1, file_out);
	
	// variable length records
	print_lasHead(las_head1, las_head2, rec_head);
	for(i=las_head1.headsize; i<las_head1.offset; i++){
		fread(&uchar, sizeof(unsigned char), 1, file_in);
		fwrite(&uchar, sizeof(unsigned char), 1, file_out);                           
	}
	printf("\n");
	
	// determine limits
	maxE = (las_head2.x_max - las_head2.x_offset)/las_head2.x_scale;
	minE = (las_head2.x_min - las_head2.x_offset)/las_head2.x_scale;
	printf("Transformed Eastings %f %f\n", minE, maxE);
	maxN = (las_head2.y_max - las_head2.y_offset)/las_head2.y_scale;
	minN = (las_head2.y_min - las_head2.y_offset)/las_head2.y_scale;
	printf("Transformed Northings %f %f\n", minN, maxN);
	maxZ = (las_head2.z_max - las_head2.z_offset)/las_head2.z_scale;
	minZ = (las_head2.z_min - las_head2.z_offset)/las_head2.z_scale;
	printf("Transformed Elevations %f %f\n\n", minZ, maxZ);	

	// open ESRI header file
	strcpy(header, argv[3]);
	strcat(header, ext1);
	group_hdr = fopen(header, "r");
	if(group_hdr == NULL){printf("unable to open file %s\n",header); return 1;}
	printf("Successfully opened file %s\n", header);

	// read header
	grid_type = atoi(argv[4]);
	
	if(grid_type == 1){ // type = new
		read_esri_new(group_hdr, &esri_new);
		print_esri_new(esri_new);
		rows = esri_new.nrows;
		cols = esri_new.ncols;
		cellsize = esri_new.xdim;
		nodata_value = esri_new.nodata;
		
	}
	if(grid_type == 0){ // type = 0 = old
		read_esri_hdr(group_hdr, &esri_hdr);
		print_esri_hdr(esri_hdr);
		rows = esri_hdr.nrows;
		cols = esri_hdr.ncols;
		cellsize = esri_hdr.cellsize;
		nodata_value = esri_hdr.nodata_value;

	}
	if(grid_type == 2){ // type = 0 = old
		read_esri_hdr(group_hdr, &esri_hdr);
		print_esri_hdr(esri_hdr);
		rows = esri_hdr.nrows;
		cols = esri_hdr.ncols;
		cellsize = esri_hdr.cellsize;
		nodata_value = esri_hdr.nodata_value;
		
	}
	
		
	//open binary files	
	// ground
	strcpy(binary, argv[3]);
	strcat(binary, ext2);
	ground_bin = fopen(binary, "rb");
	if(ground_bin == NULL){printf("unable to open file %s\n",binary); return 1;}
	printf("Successfully opened file %s\n", binary);
	
	
	// allocate memory
	ground = (float **) calloc(rows, sizeof(float *));
	if(!(ground)){printf("calloc error - rows\n"); return 1;}
	for(i=0; i<rows; i++){
		ground[i] = (float *) calloc(cols, sizeof(float));
		if(!(ground[i])){printf("calloc error - row %d",i); return 1;}
	}
	printf("Allocated memory ground %d x %d\n", rows, cols);
	for (i=rows-1; i>=0; i--) {
		for (j=0; j<cols; j++) {
			fread(&ground[i][j], sizeof(float), 1, ground_bin);
		}
		if(grid_type == 2){ // skip other two layers
			for (j=0; j<cols; j++) {
				fread(&hold, sizeof(float), 1, ground_bin);
			}
			for (j=0; j<cols; j++) {
				fread(&hold, sizeof(float), 1, ground_bin);
			}
		}
	}
	
	fclose(ground_bin);
	
	reset = atoi(argv[5]);
	if(reset)
		printf("Resetting all classifications to 0\n");
	check = atoi(argv[6]);
	if(check)
		printf("Checking existing ground points against surface\n");
	split = atoi(argv[7]);
	if(split)
		printf("Splitting class 2 into class 11 (-2 m < HAG < -1 m) and class 12 (0.3 m < HAG < 1 m)\n");
	pull = atoi(argv[8]);
	if(pull && !split)
		printf("Pulling surface down, only class 12 (-10 m < HAG < 1 m)\n");
	
	
	fseek(file_in, las_head1.offset , 0);          
	j=0;
	i=0;
	cut=0;
	
	while(j< rec_head.num_points ){
		fread(&point, rec_head.rec_len,1, file_in);
		
		rn = point.bits & ret; // find return number
		nr = (point.bits & num) >>3; // find number of returns
		if(point.p_class != 2 || reset)
			point.p_class = 0; // reset all but existing ground
		
		if(rn != rn){ // ground only lasts
			fwrite(&point, rec_head.rec_len,1, file_out);
			classes[point.p_class]++; // increment appropriate counter
			j++;
			continue;
		}
			
		row = (int) floor((point.y -  minN)*las_head2.y_scale/cellsize);
		col = (int) floor((point.x -  minE)*las_head2.x_scale/cellsize);
		
		
		if(row<0 || row>=rows || col<0 || col>=cols){
				printf("out! ");
				fprintf(record, "%d %d\t",point.x, point.y);
				fwrite(&point, rec_head.rec_len,1, file_out);
				classes[point.p_class]++; // increment appropriate counter
				j++;
			cut++;
			if(cut > 100){
				printf("more than 100 points outside. quitting.");
				return 1;
			}
			
				continue;
		}
		if(ground[row][col]==nodata_value){ // no ground reference
			fwrite(&point, rec_head.rec_len,1, file_out);
			classes[point.p_class]++; // increment appropriate counter
			j++;
			continue;
		}
			
		if(point.p_class != 2 || check){
				elev = 1.0 * point.z * las_head2.z_scale + las_head2.z_offset;
				hag = elev - ground[row][col]; // height above ground
			
				
			if(split){ // assuming good surface, can't pull down more than 1 m
					if(hag >= -2 && hag <= -1)
						point.p_class = 11; // between one and two metres below ground elevation
					if(hag >= 0.3 && hag <=1)
						point.p_class = 12; // more than 30 cm, less than one metre above ground elevation
					if(hag >= -1 && hag <=0.3)
						point.p_class = 2; // no more than 30 cm above and 1 m below ground elevation
					if(check && point.p_class == 2 && (hag > 1 || hag < -1))
					   point.p_class = 1; // had been classed ground, but more than 1 m from surface
			} // end split
			else{
					if(pull){ // pull surface down
						if(hag <= 1)
							point.p_class = 12; // no more than 1 m above ground elevation
					}
					else{ // assuming good surface
						if(hag >= -1 && hag <=1)
							point.p_class = 2; // within 1 m of ground elevation
						if(check && point.p_class == 2 && (hag > 1 || hag < -1))
							point.p_class = 1; // had been classed ground, but more than 1 m from surface
					}
			}
			if(rn != nr && point.p_class != 1)
				point.p_class = 0; // not last/single, not more than 1 m
				
			if(hag < -10)
				point.p_class = 7; // low noise
			if(hag > 1000)
				point.p_class = 18; // high noise
		}
		
		fwrite(&point, rec_head.rec_len,1, file_out);
		classes[point.p_class]++; // increment appropriate counter

		j++;
	} // end while j

	
	printf("\nClasses: ");
	fprintf(record, "\nClasses: ");
	for(i=0; i<19; i++){
		printf("%d %d; ",i, classes[i]);
		fprintf(record,"%d %d; ",i, classes[i]);
	}
	printf("\n %d\n", j);
	fprintf(record, "\n\n");
	
	free(ground);
	fclose(file_in);
	fclose(file_out);
	
	
	
    return 0;
}
