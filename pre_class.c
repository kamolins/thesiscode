#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include "LAS_structures.h"
#include "read_las.h"
#include "esri_hdr.h"


/**
 * Preliminary clasification
 * Classify medium and high vegetation points as 14 and 15 (i.e., not final)
 * Also classifies rooftops as 16
 * Ground must already be classified.
 * Mask for separating buildings from trees. 
 **/

int main (int argc, const char * argv[]) {
	FILE *file_in, *file_out; // las files
	FILE *rugged_hdr, *rugged_bin; // ruggedness index
	FILE *ground_hdr, *ground_bin; // ground approximation
	FILE *record;
	
	char header[100], binary[100]; // file names
	char ext1[]=".hdr", ext2[]=".bil"; // file extensions
	
	Header1 las_head1; // system info etc
	Header2 las_head2; // point transformation info
	Rec_head rec_head; // point record info
	Point1 point;
	ESRI_hdr esri_hdr, esri_rug; // grid file header
	//ESRI_new esri_new; // grid file header
	
	unsigned char ret = 0x07, num = 0x38, rn, nr; // point return number
	unsigned char uchar;
	int i,j; 
	int row, col, rR, cR;
	
	int classes[22]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};	
	
	double maxE, minE, maxN, minN, maxZ, minZ; // limits
	float **ground;
	int **rugged;
	float elev, hag;
	//double px, py;
	int mode;
	
	struct tm *ptr;
	time_t tm;
	tm = time(NULL);
	ptr = localtime(&tm);
	
	// command line; (name) (file in) (file out)...
	if(argc !=6) {
		printf( "usage: %s <input> <output> <ground> <mask> <mask mode>\n", argv[0] );
		printf("where input and output are LAS, ground is ESRI binary rasters - omit extension\n");
		printf("ground bald earth (no objects, filled)\n");
		printf("mask mode 0: based on TRI, cleaned up so count of how many low TRI touching cell where high count indicates flat\n");
		printf("mask mode 1: based on multiple returns and height differences, cleaned up 0 1 2 where 2 indicates building\n");
		return 0;
	}
	
	record = fopen("/Users/km.amolins/code/classveg_log.txt","a");
	fprintf(record, asctime(ptr));
	fprintf(record, "%s %s %s %s %s %s\n",argv[0], argv[1], argv[2], argv[3], argv[4], argv[5]); // function
	
	// input las file
	file_in = fopen( argv[1], "rb" );
	if(file_in == NULL){ 
		printf("unable to open file %s\n", argv[1]); 
		system("pause");
		return 1;
	} 
	printf("Successfully opened input file %s\n", argv[1]);
	
	// output las file
	file_out = fopen( argv[2], "wb+" );
	if(file_out == NULL){ 
		printf("unable to open file %s\n", argv[2]); 
		system("pause");
		return 1;
	} 
	printf("Successfully opened output file %s\n", argv[2]);
	
	printf("LAS Files open.\n");
	
	// system and collection info
	printf("Reading Header1\n");
	fread(&las_head1, sizeof(Header1),1, file_in);
	fwrite(&las_head1, sizeof(Header1), 1, file_out);
	
	// point record info
	read_recHead(file_in, &rec_head);
	write_recHead(file_out, &rec_head);
	
	// point transformation values (scale, offset) and max/min
	printf("Reading Header2\n");
	fread(&las_head2, sizeof(Header2),1, file_in);
	fwrite(&las_head2, sizeof(Header2), 1, file_out);
	
	// variable length records
	print_lasHead(las_head1, las_head2, rec_head);
	for(i=las_head1.headsize; i<las_head1.offset; i++){
		fread(&uchar, sizeof(unsigned char), 1, file_in);
		fwrite(&uchar, sizeof(unsigned char), 1, file_out);                           
	}
	printf("\n");
	
	// determine limits
	maxE = (las_head2.x_max - las_head2.x_offset)/las_head2.x_scale;
	minE = (las_head2.x_min - las_head2.x_offset)/las_head2.x_scale;
	printf("Transformed Eastings %f %f\n", minE, maxE);
	maxN = (las_head2.y_max - las_head2.y_offset)/las_head2.y_scale;
	minN = (las_head2.y_min - las_head2.y_offset)/las_head2.y_scale;
	printf("Transformed Northings %f %f\n", minN, maxN);
	maxZ = (las_head2.z_max - las_head2.z_offset)/las_head2.z_scale;
	minZ = (las_head2.z_min - las_head2.z_offset)/las_head2.z_scale;
	printf("Transformed Elevations %f %f\n\n", minZ, maxZ);	
	
	
	// open ground header file
	strcpy(header, argv[3]);
	strcat(header, ext1);
	ground_hdr = fopen(header, "r");
	if(ground_hdr == NULL){printf("unable to open file %s\n",header); return 1;}
	printf("Successfully opened file %s\n", header);
	
	// read header
	read_esri_hdr(ground_hdr, &esri_hdr);
	print_esri_hdr(esri_hdr);
	
	// open ruggedness header file
	strcpy(header, argv[4]);
	strcat(header, ext1);
	rugged_hdr = fopen(header, "r");
	if(rugged_hdr == NULL){printf("unable to open file %s\n",header); return 1;}
	printf("Successfully opened file %s\n", header);
	
	// read header - rugged is old format
	read_esri_hdr(rugged_hdr, &esri_rug);
	print_esri_hdr(esri_rug);
	
	// close header files
	fclose(ground_hdr);
	fclose(rugged_hdr);
	
	//open binary files	
	// ground
	strcpy(binary, argv[3]);
	strcat(binary, ext2);
	ground_bin = fopen(binary, "rb");
	if(ground_bin == NULL){printf("unable to open file %s\n",binary); return 1;}
	printf("Successfully opened file %s\n", binary);
	
	// ruggedness
	strcpy(binary, argv[4]);
	strcat(binary, ext2);
	rugged_bin = fopen(binary, "rb");
	if(rugged_bin == NULL){printf("unable to open file %s\n",binary); return 1;}
	printf("Successfully opened file %s\n", binary);
	
	
	// allocate memory
	ground = (float **) calloc(esri_hdr.nrows, sizeof(float *));
	if(!(ground)){printf("calloc error - rows\n"); return 1;}
	for(i=0; i<esri_hdr.nrows; i++){
		ground[i] = (float *) calloc(esri_hdr.ncols, sizeof(float));
		if(!(ground[i])){printf("calloc error - row %d",i); return 1;}
	}
	printf("Allocated memory ground %0.0f x %0.0f\n", esri_hdr.nrows, esri_hdr.ncols);
	
	// int, cleaned up rugged
	rugged = (int **) calloc(esri_rug.nrows, sizeof(int *));
	if(!(rugged)){printf("calloc error - rows\n"); return 1;}
	for(i=0; i<esri_rug.nrows; i++){
		rugged[i] = (int *) calloc(esri_rug.ncols, sizeof(int));
		if(!(rugged[i])){printf("calloc error - row %d",i); return 1;}
	}
	printf("Allocated memory ground %0.0f x %0.0f\n", esri_rug.nrows, esri_rug.ncols);
	
	// read in data and close binary files
	for (i=esri_hdr.nrows-1; i>=0; i--) {
		for (j=0; j<esri_hdr.ncols; j++) {
			fread(&ground[i][j], sizeof(float), 1, ground_bin);
			
		}
	}
	for (i=esri_rug.nrows-1; i>=0; i--) {
		for (j=0; j<esri_rug.ncols; j++) {
			fread(&rugged[i][j], sizeof(int), 1, rugged_bin);
			
		}
	}
	
	fclose(ground_bin);
	fclose(rugged_bin);
	
	mode = atoi(argv[5]);
	if(mode)
		printf("Using mask from height differences and multiple returns.\n");
	else
		printf("Using mask from TRI.\n");
	
	fseek(file_in, las_head1.offset , 0);          
	j=0;
	
	while(j< rec_head.num_points ){
		fread(&point, rec_head.rec_len,1, file_in);
		
		rn = point.bits & ret; // find return number
		nr = (point.bits & num) >>3; // find number of returns
		
		row = (int) floor((point.y -  minN)*las_head2.y_scale/esri_hdr.cellsize);
		col = (int) floor((point.x -  minE)*las_head2.x_scale/esri_hdr.cellsize);
		
		rR = (int) floor((point.y -  minN)*las_head2.y_scale/esri_rug.cellsize);
		cR = (int) floor((point.x -  minE)*las_head2.x_scale/esri_rug.cellsize);
		
		
		if(row<0 || row>=esri_hdr.nrows || col<0 || col>=esri_hdr.ncols){
			printf("out! off ground ");
			fwrite(&point, rec_head.rec_len,1, file_out);
			classes[point.p_class]++;
			j++;
			continue;
		}
		if(rR<0 || rR>=esri_rug.nrows || cR<0 || cR>=esri_rug.ncols){
			printf("out! off rugged");
			fwrite(&point, rec_head.rec_len,1, file_out);
			classes[point.p_class]++;
			j++;
			continue;
		}
		
		if(ground[row][col]==esri_hdr.nodata_value){ // no ground reference
			fwrite(&point, rec_head.rec_len,1, file_out);
			j++;
			continue;
		}
		
		if(point.p_class != 2 && point.p_class != 7 && point.p_class !=18){ // assume just ground and noise correct
			elev = 1.0 * point.z * las_head2.z_scale + las_head2.z_offset; // real Z value  
			hag = elev - ground[row][col]; // height above ground
			
			if(mode){ // using mask from height differences
				//2 is buildings: min in above ground, no multis, and little max/min difference
				
				if(rugged[rR][cR] == 0){
					if(hag >= 0.3 && hag < 2)
						point.p_class = 14; // within specifications for medium vegetation
					if(hag >= 2)
						point.p_class = 15; // high vegetation
				}
				else{
					if(hag > 2 && rugged[rR][cR] == 2 && rn == nr)
						point.p_class = 16; // building
				}
			} // end mode 1
			
			else{ // using mask from TRI
				if(rugged[rR][cR] == 0){
					if(hag >= 0.3 && hag < 2)
						point.p_class = 14; // within specifications for medium vegetation
					if(hag >= 2)
						point.p_class = 15; // high vegetation
				}
				else{
					if(hag > 2 && rugged[rR][cR] > 6 && rn == nr)
						point.p_class = 16; // building
				}
			} // end mode 0
			
			if(hag < -10)
				point.p_class = 7; // low noise
			if(hag > 1000)
				point.p_class = 18; // high noise
		} // end check class
		
		
		fwrite(&point, rec_head.rec_len,1, file_out);
		classes[point.p_class]++; // increment appropriate counter
		
		j++;
	} // end while in points		
	
	printf("\nClasses: ");
	fprintf(record, "\nClasses: ");
	for(i=0; i<19; i++){
		printf("%d %d; ",i, classes[i]);
		fprintf(record,"%d %d; ",i, classes[i]);
	}
	printf("\n");
	fprintf(record, "\n\n");
	
	
	free(ground);
	free(rugged);
	fclose(file_in);
	fclose(file_out);
	fclose(record);
	
	
	return 0;
}
