#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include "LAS_structures.h"
#include "read_las.h"
#include "esri_hdr.h"

/**
 Reset any non-ground points within area of uncertainty to 0
 **/
int main (int argc, const char * argv[]) {
	FILE *file_in, *file_out; // las files
	FILE *ground_hdr;
	FILE *ground_bin; // ground approximation
	FILE *record;
	
	char header[100], binary[100]; // file names
	char ext1[]=".hdr", ext2[]=".bil"; // file extensions
	
	Header1 las_head1; // system info etc
	Header2 las_head2; // point transformation info
	Rec_head rec_head; // point record info
	Point1 point;
	ESRI_hdr esri_hdr; // grid file header
	ESRI_new esri_new;
	int grid_type, rows, cols;
	float cellsize, nodata_value;
	
	unsigned char ret = 0x07, num = 0x38, rn, nr; // point return number
	unsigned char uchar;
	int i,j; 
	int row, col;
	int cut;
	
	int c_old[22]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};	
	int c_new[22]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};	
	
	double maxE, minE, maxN, minN, maxZ, minZ; // limits
	float **mask;
	
	struct tm *ptr;
	time_t tm;
	tm = time(NULL);
	ptr = localtime(&tm);
	
	
	// command line; (name) (file in) (file out)...
	if(argc !=5) {
		printf( "usage: %s <input> <output> <uncertainty mask> <grid type> \n", argv[0] );
		printf("where input and output are LAS, uncertainty is ESRI binary raster - omit extension\n");
		printf("grid type 0 = old, 1 = new\n");
		return 0;
	}
	
	record = fopen("/Users/km.amolins/code/uncertainty_log.txt","a");
	fprintf(record, asctime(ptr));
	fprintf(record, "%s %s %s %s %s\n",argv[0], argv[1], argv[2], argv[3], argv[4]); // function

	
	// input las file
	file_in = fopen( argv[1], "rb" );
	if(file_in == NULL){ 
		printf("unable to open file %s\n", argv[1]); 
		system("pause");
		return 1;
	} 
	printf("Successfully opened input file %s\n", argv[1]);
	
	// output las file
	file_out = fopen( argv[2], "wb+" );
	if(file_out == NULL){ 
		printf("unable to open file %s\n", argv[2]); 
		system("pause");
		return 1;
	} 
	printf("Successfully opened output file %s\n", argv[2]);
	
	printf("LAS Files open.\n");
	
	// system and collection info
	printf("Reading Header1\n");
	fread(&las_head1, sizeof(Header1),1, file_in);
	fwrite(&las_head1, sizeof(Header1), 1, file_out);
	
	// point record info
	read_recHead(file_in, &rec_head);
	write_recHead(file_out, &rec_head);
	
	// point transformation values (scale, offset) and max/min
	printf("Reading Header2\n");
	fread(&las_head2, sizeof(Header2),1, file_in);
	fwrite(&las_head2, sizeof(Header2), 1, file_out);
	
	// variable length records
	print_lasHead(las_head1, las_head2, rec_head);
	for(i=las_head1.headsize; i<las_head1.offset; i++){
		fread(&uchar, sizeof(unsigned char), 1, file_in);
		fwrite(&uchar, sizeof(unsigned char), 1, file_out);                           
	}
	printf("\n");
	
	// determine limits
	maxE = (las_head2.x_max - las_head2.x_offset)/las_head2.x_scale;
	minE = (las_head2.x_min - las_head2.x_offset)/las_head2.x_scale;
	printf("Transformed Eastings %f %f\n", minE, maxE);
	maxN = (las_head2.y_max - las_head2.y_offset)/las_head2.y_scale;
	minN = (las_head2.y_min - las_head2.y_offset)/las_head2.y_scale;
	printf("Transformed Northings %f %f\n", minN, maxN);
	maxZ = (las_head2.z_max - las_head2.z_offset)/las_head2.z_scale;
	minZ = (las_head2.z_min - las_head2.z_offset)/las_head2.z_scale;
	printf("Transformed Elevations %f %f\n\n", minZ, maxZ);	
	
	// open ESRI header file
	strcpy(header, argv[3]);
	strcat(header, ext1);
	ground_hdr = fopen(header, "r");
	if(ground_hdr == NULL){printf("unable to open file %s\n",header); return 1;}
	printf("Successfully opened file %s\n", header);
	
	// read header
	grid_type = atoi(argv[4]);
	
	if(grid_type == 1){ // type = new
		read_esri_new(ground_hdr, &esri_new);
		print_esri_new(esri_new);
		rows = esri_new.nrows;
		cols = esri_new.ncols;
		cellsize = esri_new.xdim;
		nodata_value = esri_new.nodata;
		
	}
	if(grid_type == 0){ // type = 0 = old
		read_esri_hdr(ground_hdr, &esri_hdr);
		print_esri_hdr(esri_hdr);
		rows = esri_hdr.nrows;
		cols = esri_hdr.ncols;
		cellsize = esri_hdr.cellsize;
		nodata_value = esri_hdr.nodata_value;
		
	}
	
	//open binary files	
	// ground
	strcpy(binary, argv[3]);
	strcat(binary, ext2);
	ground_bin = fopen(binary, "rb");
	if(ground_bin == NULL){printf("unable to open file %s\n",binary); return 1;}
	printf("Successfully opened file %s\n", binary);
	
	
	// allocate memory
	mask = (float **) calloc(rows, sizeof(float *));
	if(!(mask)){printf("calloc error - rows\n"); return 1;}
	for(i=0; i<rows; i++){
		mask[i] = (float *) calloc(cols, sizeof(float));
		if(!(mask[i])){printf("calloc error - row %d",i); return 1;}
	}
	printf("Allocated memory ground %d x %d\n", rows, cols);
	for (i=rows-1; i>=0; i--) {
		for (j=0; j<cols; j++) {
			fread(&mask[i][j], sizeof(float), 1, ground_bin);
		}
	}
	
	fclose(ground_bin);
	
	fseek(file_in, las_head1.offset , 0);          
	j=0;
	i=0;
	cut=0;
	
	while(j< rec_head.num_points ){
		fread(&point, rec_head.rec_len,1, file_in);
		j++; // so don't get caught in endless loop
		
		rn = point.bits & ret; // find return number
		nr = (point.bits & num) >>3; // find number of returns
		
		if(point.p_class == 2 || point.p_class == 7 || point.p_class == 18){ 
			// assume ground and noise correct
			fwrite(&point, rec_head.rec_len,1, file_out);
			c_old[point.p_class]++; // increment appropriate counter
			c_new[point.p_class]++; // increment appropriate counter
			continue;
		}
		
		row = (int) floor((point.y -  minN)*las_head2.y_scale/cellsize);
		col = (int) floor((point.x -  minE)*las_head2.x_scale/cellsize);
		
		if(row<0 || row>=rows || col<0 || col>=cols){
			printf("out! ");
			fprintf(record, "%d %d\t",point.x, point.y);
			fwrite(&point, rec_head.rec_len,1, file_out);
			c_old[point.p_class]++; // increment appropriate counter
			c_new[point.p_class]++; // increment appropriate counter
			cut++;
			if(cut > 100){
				printf("more than 100 points outside. quitting.");
				return 1;
			}
				
			continue;
		}
		if(mask[row][col]!=1){ // not in masked area
			fwrite(&point, rec_head.rec_len,1, file_out);
			c_old[point.p_class]++; // increment appropriate counter
			c_new[point.p_class]++; // increment appropriate counter
			continue;
		}
		
		// at this point, non-ground or noise where mask is 1
		c_old[point.p_class]++; 
		point.p_class = 0; // reset
		c_new[point.p_class]++; 
		
		fwrite(&point, rec_head.rec_len,1, file_out);

		
	} // end while
	
	printf("\nOld Classes: ");
	fprintf(record, "\nOld Classes: ");
	for(i=0; i<19; i++){
		printf("%d %d; ",i, c_old[i]);
		fprintf(record,"%d %d; ",i, c_old[i]);
	}
	printf("\n");
	fprintf(record, "\n");

	printf("\nNew Classes: ");
	fprintf(record, "\nNew Classes: ");
	for(i=0; i<19; i++){
		printf("%d %d; ",i, c_new[i]);
		fprintf(record,"%d %d; ",i, c_new[i]);
	}
	printf("\n %d\n", j);
	fprintf(record, "\n\n");
	
	free(mask);
	fclose(file_in);
	fclose(file_out);
	
	
	return 0;
}
