#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "esri_hdr.h"

/****************************
 Applies B3 cubic spline (5x5) wavelet filter.
 Output: approxmation image and detail image.
 Dyadic resolution: determines which cells are touched by the filter. 1 = every cell; 2 = every other; etc.
 ****************************/

int main (int argc, const char * argv[]) {
	FILE *hdr_in, *bin_in, *hdr_outA, *bin_outA, *hdr_outD, *bin_outD;
	ESRI_hdr esri_hdr;
	char header[100], binary[100]; // file names
	char ext1[]=".hdr", ext2[]=".bil"; // file extensios
	char type1[]="_A", type2[]="_D"; // file extensios
	float **input, **approx, **detail; // data
	float	b3_spline[5][5] = {{1,4,6,4,1},{4,16,24,16,4},{6,24,36,24,6}, {4,16,24,16,4},{1,4,6,4,1}};
	int i, j, k,l, res;
	float divisor;
	
	// command line: (name) (input file) (output file) (resolution)
	if(argc !=4){
		printf("usage: %s <input> <output> <dyadic resolution>\n", argv[0]);
		printf("Input and output are ESRI rasters.\n");
		printf("dyadic resolution: 1 = first approximation, 2 = second approximation, etc.\n");
		return 0;
	}
	
	//open header files
	strcpy(header, argv[1]);
	strcat(header, ext1);
	hdr_in = fopen(header, "r");
	if(hdr_in == NULL){printf("unable to open file %s\n",header); return 1;}
	printf("Successfully opened file %s\n", header);
	
	strcpy(header, argv[2]);
	strcat(header, type1);
	strcat(header, ext1);
	hdr_outA = fopen(header, "w");
	if(hdr_outA == NULL){printf("unable to open file %s\n",header); return 1;}
	printf("Successfully opened file %s\n", header);

	strcpy(header, argv[2]);
	strcat(header, type2);
	strcat(header, ext1);
	hdr_outD = fopen(header, "w");
	if(hdr_outD == NULL){printf("unable to open file %s\n",header); return 1;}
	printf("Successfully opened file %s\n", header);
	
	// read header
	read_esri_hdr(hdr_in, &esri_hdr);
	print_esri_hdr(esri_hdr);
	
	// write esri header
	write_esri_hdr(hdr_outA, &esri_hdr);
	write_esri_hdr(hdr_outD, &esri_hdr);
	
	// close header files
	fclose(hdr_in);
	fclose(hdr_outA);
	fclose(hdr_outD);
	
	//open binary files	
	strcpy(binary, argv[1]);
	strcat(binary, ext2);
	bin_in = fopen(binary, "rb");
	if(bin_in == NULL){printf("unable to open file %s\n",binary); return 1;}
	printf("Successfully opened file %s\n", binary);
	
	strcpy(binary, argv[2]);
	strcat(binary, type1);
	strcat(binary, ext2);
	bin_outA = fopen(binary, "wb");
	if(bin_outA == NULL){printf("unable to open file %s\n",binary); return 1;}	
	printf("Successfully opened file %s\n", binary);
	
	strcpy(binary, argv[2]);
	strcat(binary, type2);
	strcat(binary, ext2);
	bin_outD = fopen(binary, "wb");
	if(bin_outD == NULL){printf("unable to open file %s\n",binary); return 1;}	
	printf("Successfully opened file %s\n", binary);

	res = atoi(argv[3]);
	printf("Resolution 2^%d\n", res);
	
	// allocate memory
	input = (float **) calloc(esri_hdr.nrows, sizeof(float *));
	if(!(input)){printf("calloc error - rows\n"); return 1;}
	for(i=0; i<esri_hdr.nrows; i++){
		input[i] = (float *) calloc(esri_hdr.ncols, sizeof(float));
		if(!(input[i])){printf("calloc error - row %d",i); return 1;}
	}
	approx = (float **) calloc(esri_hdr.nrows, sizeof(float *));
	if(!(approx)){printf("calloc error - rows\n"); return 1;}
	for(i=0; i<esri_hdr.nrows; i++){
		approx[i] = (float *) calloc(esri_hdr.ncols, sizeof(float));
		if(!(approx[i])){printf("calloc error - row %d",i); return 1;}
	}
	detail = (float **) calloc(esri_hdr.nrows, sizeof(float *));
	if(!(detail)){printf("calloc error - rows\n"); return 1;}
	for(i=0; i<esri_hdr.nrows; i++){
		detail[i] = (float *) calloc(esri_hdr.ncols, sizeof(float));
		if(!(detail[i])){printf("calloc error - row %d",i); return 1;}
	}
	
	printf("Allocated memory 3x %0.0f x %0.0f\n", esri_hdr.nrows, esri_hdr.ncols);
	
	// read data into grid
	for(i=0; i<esri_hdr.nrows; i++){
		for(j=0; j<esri_hdr.ncols; j++){
			fread(&input[i][j], sizeof(float), 1, bin_in);
		}
	}
	printf("Data read\n");
	
		
	// apply filter
	for(i=0; i<esri_hdr.nrows; i++){
		for(j=0; j<esri_hdr.ncols; j++){
			divisor = 256.0;
			approx[i][j]=0;
			for(k=0; k<5; k++){
				if(i+res*(k-2)<0 || i+res*(k-2)>=esri_hdr.nrows){
					for(l=0; l<5; l++){
						divisor = divisor - b3_spline[k][l];
					}
					continue;
				}
				
				
				for(l=0; l<5; l++){
					if(j+res*(l-2)<0 || j+res*(l-2)>=esri_hdr.ncols){
						divisor = divisor - b3_spline[k][l];
						continue;
					}
					if(input[i+res*(k-2)][j+res*(l-2)]<=esri_hdr.nodata_value){
						divisor = divisor - b3_spline[k][l];
						continue;
					}
					if(divisor == 0)
						divisor = 1;
					approx[i][j] += input[i+res*(k-2)][j+res*(l-2)] * b3_spline[k][l];				
				} // end l
			} // end k
			approx[i][j] = approx[i][j] / divisor;
			if(input[i][j]<=esri_hdr.nodata_value)
				detail[i][j] = esri_hdr.nodata_value;
			else
				detail[i][j] = input[i][j] - approx[i][j];
		} // end j
	} // end i
	printf("Wavelet filter applied\n");
	
	// write approximation and detail
	for (i=0; i<esri_hdr.nrows; i++) {
		for (j=0; j<esri_hdr.ncols; j++) {
			fwrite(&approx[i][j], sizeof(float), 1, bin_outA);
			fwrite(&detail[i][j], sizeof(float), 1, bin_outD);
		}
	}
	printf("Data written to file\n");
	
	//close
	fclose(bin_in);
	fclose(bin_outA);
	fclose(bin_outD);
	
} // end main
	
	
